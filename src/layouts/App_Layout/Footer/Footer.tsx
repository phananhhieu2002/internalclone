import { Layout } from "antd";
import styles from './Footer.module.css';
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import styled from "styled-components";
import {memo} from "react";
const FooterWrapper = styled.div`
  margin-top: 75px;
  .ant-layout-footer{
    padding: 24px 0;
  }
`
const Footer = () => {
    const themeColor = useSelector((state : RootState) => state.theme.value);
    return (
        <FooterWrapper>
            <Layout.Footer className={styles.layoutFooter} style={themeColor ? {color : "var(--light)"} : {color : "var(--dark)"}}>
                Copyright © 2023 NIS All rights reserved.
            </Layout.Footer>
        </FooterWrapper>
    )
}
export default memo(Footer);