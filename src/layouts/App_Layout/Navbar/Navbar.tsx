import styles from './Navbar.module.css';
import { MenuFoldOutlined, BellOutlined, SettingOutlined, UserOutlined, LogoutOutlined , MenuUnfoldOutlined } from '@ant-design/icons';
import React, { useRef, useState} from 'react';
import { Dropdown, Space, Card, Menu, Button, Drawer } from 'antd';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux'
import {push,pull} from '@/redux/slices/app.slice';
import logo from "@/public/assets/logo.png";
import logoSm from "@/public/assets/logo-sm.png";
import logoWhite from "@/public/assets/logo-white.png";
import Image from "next/image";
import ThemeColor from "@/components/navbar/ThemeColor";
import CategoryColor from "@/components/navbar/categoryColor";
import {RootState} from "@/redux/store";
import {useRouter} from "next/router";
import {memo} from "react";
import Utils from "@/utils";

const notificationList = (
    <div className="nav-dropdown nav-notification">
        <Card type="inner" title="Notifications" extra={<a href="">More</a>}>
            <div className={styles.notificationPane}>
                <div className={styles.emptyNotification}>
                    <img src="https://gw.alipayobjects.com/zos/rmsportal/sAuJeJzSKbUmHfBQRzmZ.svg" alt="empty" />
                    <p className={styles.text}>You have viewed all notifications</p>
                </div>
            </div>
        </Card>
    </div>
);
const NavbarBox = styled.div`
    .nav-notification .ant-card{
        box-shadow: 0 0 6px #333 !important;
    }
  .anticon{
        transition: all 1s;
  }
    .anticon:hover{
        color: #3e79f7;
    }
  .ant-radio-group .ant-radio-button-wrapper{
    padding-inline: 5px ;
  }
`;
const SettingWrapper = styled.div`
    padding: 24px;
`

const Navbar = () => {
    const user = useSelector((state : RootState) => state.user);
    const [open, setOpen] = useState(false);
    const categoryStatus = useSelector((state:RootState) => state.app.bgrStatus);
    const themeStatus = useSelector((state : RootState) => state.theme.value)
    const dispatch = useDispatch();
    const router = useRouter()
    const menuProfile = (
      <div className={styles.profile}>
          <Menu
            items={[
                {
                    label: <div className='infomationUser'>
                        <h1 className='nameUser' style={{fontWeight : 700}}>{user.fullName}</h1>
                        <small className='roleUser'>{Utils.parseFirstCharacter(user.role)}</small>
                    </div>,
                    key: "profile",
                    icon: <div className={styles.user}>
                        <img src={user.avatar} alt=""/>
                    </div>,
                },
                {
                    label: "Thông tin cá nhân",
                    key: "info",
                    icon: <UserOutlined />,
                },
                {
                    label: "Cài đặt tài khoản cá nhân",
                    key: "settings",
                    icon: <SettingOutlined />,
                },
                {
                    label: "Đăng xuất",
                    key: "signout",
                    icon: <LogoutOutlined />,
                },
            ]}
          >
              <h1>HieuDev</h1>
          </Menu>
      </div>
    )
    const showDrawer = () => {
        setOpen(true);
    }
    const hideDrawer = () => {
        setOpen(false);
    }
    const handleToggleCategory = () => {
        categoryStatus ? dispatch(pull()) : dispatch(push());
    }
    const NavbarColorStyle = themeStatus ? {color : "var(--light)" } : {color : "var(--dark)"}
    const NavBarBackgroundStyle = themeStatus ? {backgroundColor : "var(--darkBackground)"} : {backgroundColor : "var(--light)" }
    const logoImage = themeStatus ? logoWhite : logo;
    return (

        <NavbarBox
            className={styles.main}
            style={{...NavBarBackgroundStyle,...NavbarColorStyle}}
        >
            <div className={styles.logo}  style={categoryStatus ? {width : 283} : {width : 65}}>
                {/*<a href="/">*/}
                    <Image
                        className={styles.img}
                        src={categoryStatus ? logoImage : logoSm}
                        style={categoryStatus ? {objectFit : 'contain'} : {objectFit : 'cover'}}
                        onClick={() => router.reload()  }
                    />
                {/*</a>*/}
            </div>
            <div className={styles.navigation} style={{"width" : "100%"}}>
                <div className={styles.menu} onClick={handleToggleCategory}>
                    {categoryStatus ? <MenuFoldOutlined/> : <MenuUnfoldOutlined />}
                </div>
                <div className={styles.navList}>
                    <Dropdown
                        placement="bottomRight"
                        overlay={notificationList}
                        trigger={["click"]}
                    >
                        <Space>
                            <div className={styles.navItem}>
                                <BellOutlined />
                            </div>
                        </Space>
                    </Dropdown>

                    <div className={styles.navItem}>
                        <SettingOutlined onClick={showDrawer} />
                        <Drawer
                            title="Theme Config"
                            placement="right"
                            onClose={hideDrawer}
                            open={open}
                        >
                            <SettingWrapper>
                                <ThemeColor />
                                <CategoryColor />
                            </SettingWrapper>
                        </Drawer>
                    </div>
                    <Dropdown
                        overlay={menuProfile}
                        trigger={["click"]}
                    >
                        <div className={styles.navItem}>
                            <div className={styles.user}>
                                <img src={user.avatar} alt=""/>
                            </div>
                        </div>
                    </Dropdown>
                </div>
            </div>
        </NavbarBox>
    )
}
export default memo(Navbar);