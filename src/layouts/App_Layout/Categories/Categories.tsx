import styles from './Categories.module.css';
import styled from 'styled-components';
import {useRouter} from "next/router";
import  {
    HomeOutlined
    , UsergroupAddOutlined
    , FileOutlined
    , UserOutlined
    , MailOutlined
    , ProfileOutlined
    , CalendarOutlined
    , CheckSquareOutlined
    , PaperClipOutlined
    , FileSyncOutlined
    , ContainerOutlined
    , SolutionOutlined
    , VerticalAlignMiddleOutlined
} from '@ant-design/icons';
import React, {useState, useEffect} from 'react';
import type {MenuProps} from 'antd';
import {Menu} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import {memo} from "react";
import {setPage} from "@/redux/slices/app.slice";

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}
const items: MenuItem[] = [
    getItem('Home', '/home', <HomeOutlined/>),
    getItem('Nhóm', '/groups', <UsergroupAddOutlined/>),
    getItem('Đăng bài', '/posts', <FileOutlined/>, []),
    getItem('HRM', '/hrm', <UserOutlined/>, [
        getItem('Yêu cầu', '/hrm/request', <FileSyncOutlined/>),
        getItem('Báo cáo', '/hrm/report/new', <ContainerOutlined/>),
        getItem('Đánh giá nhân sự', '/hrm/feedback', <SolutionOutlined/>),
        getItem('Quy định', '/hrm/payment-regulation/bonus', <VerticalAlignMiddleOutlined/>),
    ]),
    getItem('Mail', '/mails', <MailOutlined/>, [
        getItem('Quản lý hộp thư', '/mails/emails', <MailOutlined/>),
        getItem('Quản lý người nhận ', '/mails/receivers', <UserOutlined/>),
    ]),
    getItem('Project', '/projects', <ProfileOutlined/>),
    getItem('Thời gian biểu', '/timeline', <CalendarOutlined/>),
    getItem('Nhiệm vụ', '/tasks', <CheckSquareOutlined/>),
    getItem('Tài liệu', '/documents', <PaperClipOutlined/>),
];
const Category = styled.div`
  .ant-menu-item-selected {
    border-right: 2px solid #3e79f7;
    transition: all 1s;
  }

  .ant-menu-item , .ant-menu-submenu {
    width: 100%;
    margin: 0;
    border-radius: 0;
    margin: 8px 0;
  }

  .ant-menu-item-selected {
  }

  .ant-menu-submenu,
  .ant-menu-submenu-title {
    width: 100%;
    margin: 8px 0;
    border-radius: 0;
  }

  .ant-menu-item:hover {
    color: #3e79f7 !important;
    background-color: transparent !important;
  }

  .ant-menu-submenu-title:hover {
    color: #3e79f7 !important;
    background-color: transparent !important;
  }

  .ant-menu-item-icon {
    font-size: 17px !important;
  }

  .ant-menu-title-content {
    width: 0;
    font-size: 14px;
  }
  :where(.css-dev-only-do-not-override-12jzuas).ant-menu-light, :where(.css-dev-only-do-not-override-12jzuas).ant-menu-light>.ant-menu {
    background: transparent !important;
  }
    .ant-menu-title-content{
        font-weight: 500;
    }
`;
const Categories = () => {
    const router = useRouter();
    const items: MenuItem[] = [
        getItem('Home', '/home', <HomeOutlined/>),
        getItem('Nhóm', '/groups', <UsergroupAddOutlined/>),
        getItem('Đăng bài', '/posts', <FileOutlined/>, []),
        getItem('HRM', '/hrm', <UserOutlined/>, [
            getItem('Yêu cầu', '/hrm/request', <FileSyncOutlined/>),
            getItem('Báo cáo', '/hrm/report/new', <ContainerOutlined/>),
            getItem('Đánh giá nhân sự', '/hrm/feedback', <SolutionOutlined/>),
            getItem('Quy định', '/hrm/payment-regulation/bonus', <VerticalAlignMiddleOutlined/>),
        ]),
        getItem('Project', '/projects', <ProfileOutlined/>),
        getItem('Mail', '/mail', <MailOutlined/>, [
            getItem('Quản lý hộp thư', '/mail/manage-mails/inbox', <MailOutlined/>),
            getItem('Quản lý người nhận ', '/mail/manage-receivers', <UserOutlined/>),
        ]),
        getItem('Thời gian biểu', '/timeline', <CalendarOutlined/>),
        getItem('Nhiệm vụ', '/tasks', <CheckSquareOutlined/>),
        getItem('Tài liệu', '/documents', <PaperClipOutlined/>),
    ];
    const themeStatus  = useSelector((state : RootState) => state.theme.value);
    const rootSubmenuKeys = ['posts', 'hrm', 'projects'];
    const [openKeys, setOpenKeys] = useState(['posts']);
    const statusCategory = useSelector((state: RootState) => state.app.bgrStatus);
    const colorCategory = useSelector((state:RootState) => state.app.color);
    const dispatch = useDispatch();
    const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
        const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey!) === -1) {
            setOpenKeys(keys);
        } else {
            setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };
    useEffect(() => {
        const arrows = document.querySelectorAll('.ant-menu-submenu-arrow');
        arrows.forEach(arrow => {
            statusCategory ? arrow.style.display = "block" : arrow.style.display = "none";
        })
    }, [statusCategory])
    const onClick: MenuProps['onClick'] = (e) => {
        router.replace(e.key)
        dispatch(setPage(1));
    };
    const widthCategoryStyle = statusCategory ? {width: 250} : {width: 65};
    const BackGroundColorCategoryStyle = colorCategory === "Dark" || themeStatus  ? {backgroundColor : "var(--darkBackground) "} : {backgroundColor : "var(--light)"};
    const ColorCategoryStyle = colorCategory === "Dark" || themeStatus ? {color : "var(--light)"} : {color : "var(--dark)"};
    return (
        <Category
            className={styles.categories}
            style={{...widthCategoryStyle , ...BackGroundColorCategoryStyle  }}
        >
            <ul className={styles.categoryList}>
                <Menu
                    mode="inline"
                    onClick={onClick}
                    openKeys={openKeys}
                    onOpenChange={onOpenChange}
                    defaultSelectedKeys={[`${router.pathname}`]}
                    className={styles.menuItem}
                    items={items}
                    style={{...ColorCategoryStyle }}
                />
            </ul>
        </Category>
    )
}

export default memo(Categories);