import {memo, PropsWithChildren} from "react";
import Navbar from "./Navbar/Navbar";
import Categories from "./Categories/Categories";
import Footer from "./Footer/Footer";
import {useSelector} from "react-redux";
import type { RootState } from '@/redux/store';
const AppLayOut = ({ children }: PropsWithChildren) => {
    const themeStatus = useSelector((state : RootState) => state.theme.value)
    const categoryStatus = useSelector((state : RootState) => state.app.bgrStatus);
    const MainyMarginStyle = categoryStatus ? {marginLeft : 250} : {marginLeft : 65}
    const MainColor = themeStatus ? {color : "var(--light)"} : {color : "var(--dark)"}
    const MainBackgroundStyle = themeStatus ? {background : "var(--backgroundMain)"} : {background : "var(--appBackground)"}
    if(typeof window !== 'undefined'){
        var minHeightStyle =  {
            minHeight : `${window.innerHeight}px`
        }
    }
    return (
        <div className="app">
            <Navbar />
            <div className="inner">
                <Categories />
                <div
                    className="content"
                    style={{...MainyMarginStyle,...MainBackgroundStyle,...MainColor,...minHeightStyle}}
                >
                    {children}
                    <Footer />
                </div>
            </div>
        </div>
    )
}
export default memo(AppLayOut);