import { NextPageWithLayout } from "../../pages/_app";
import styled from 'styled-components';
import { Button, Menu } from "antd";
import { EditOutlined, ClockCircleOutlined, CheckCircleOutlined, CloseCircleOutlined, InboxOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd/es/menu';
import React, { ReactElement } from "react";
import TaskHeader from "@/components/tasks/TaskHeader";
import { useRouter } from "next/router";
interface User {
  name: string;
  avatar: string;
}
interface DataType {
  key: string;
  title: string;
  status: ReactElement;
  user?: User;
}


const TaskContainer = styled.div`

  .show-range-time {
    display: flex;
    align-items: center;
    border: 1px solid #ccc;
    padding: 0 15px;
    border-radius: 7px;
  }

  .mr-2 {
    padding-right: 5px;
  }

  .content {
    //height: 400px;
    margin: 25px 0;
    padding: 0;
    display: flex;
    background-color: #fff
  }

  .side_content {
    margin-top: 9px;
    width: 250px;
  }

  .btn_create {
    min-width: 160px;
    max-height: 40px;
    background-color: #1677ff;
    box-sizing: content-box;
  }

  .create_box {
    padding: 16px;
    display: flex;
    justify-content: center;
  }

  .ant-menu-item {
    margin: 0;
    width: 100%;
    border-radius: 0;
    padding: 0 16px 0 24px;
    margin: 4px 0;
  }

  .ant-menu .ant-menu-item:hover {
    color: #1677ff !important;
    background-color: transparent !important;
  }

  .ant-menu-item-selected {
    border-right: 2px solid #1677ff;
  }
  .content{
    min-height: 800px;
  }
  .main_content {
    padding: 24px;
    width: calc(100% - 250px);
    margin-top: 4px;
    display : flex;
    justify-content : center;
    /* align-items: center; */
  }
    .ant-menu-title-content{
        font-weight: 600;
    }
`
type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key?: React.Key | null,
  icon?: React.ReactNode,
  children?: MenuItem[],
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
  } as MenuItem;
}
const items: MenuItem[] = [
  getItem('Tất cả', '/', <ClockCircleOutlined />),
  getItem('Đang chờ', 'todo', <ClockCircleOutlined />),
  getItem('Đang thực hiện', 'in_progress', <ClockCircleOutlined />),
  getItem('Đã hoàn thành', 'completed', <CheckCircleOutlined />),
  getItem('Đã trễ', 'due', <CloseCircleOutlined />),
  getItem('Lưu trữ', 'archived', <InboxOutlined />),
];
const TaskLayout: NextPageWithLayout<{ children: React.ReactNode }> = ({ children }) => {
  const router = useRouter();
  const handleClick: MenuProps['onClick'] = (e) => {
    router.push(`/tasks/${e.key}`)
  }
  const onButtonClick: MenuProps['onClick'] = () => {
    router.push(`/tasks/create`)
  }
  return (
    <TaskContainer className="container">
      <main className="main">
        <TaskHeader />
        <div className="content">
          <div className="side_content">
            <div className="side_content-main">
              <div className="create_box">
                <Button
                  type="primary"
                  className='btn_create'
                  onClick={onButtonClick}
                >
                  <EditOutlined />
                  Tạo task mới
                </Button>
              </div>
              <div className="menu">
                <Menu
                  style={{ width: 256 }}
                  items={items}
                  defaultSelectedKeys={['/']}
                  onClick={handleClick}
                />
              </div>
            </div>
          </div>
          <div className="main_content">
            {children}
          </div>
        </div>
      </main>
    </TaskContainer>
  )
}


export default TaskLayout;