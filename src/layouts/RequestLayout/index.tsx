import {Col, Row} from "antd";
import {PropsWithChildren} from "react";
import Menu from "@/layouts/RequestLayout/Menu";
import Option from "@/layouts/RequestLayout/Option";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";

const RequestLayout = ({children} : PropsWithChildren) => {
	return (
		<Row>
			<Col xs={24} lg={18} style={{padding : "0 8px"}}>
				<Option />
				{children}
			</Col>
			<Col xs={24} lg={6} style={{padding : "0 8px"}}>
				<Menu />
			</Col>
		</Row>
	)
}

export default RequestLayout;