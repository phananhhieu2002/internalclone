import {Avatar} from "antd";
import {SettingOutlined, UserOutlined} from "@ant-design/icons";
import { Typography } from 'antd';
import styled from "styled-components";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";
const { Title } = Typography;
const HeaderContainer = styled.div`
	.infoPart{
		display: flex;
	}
	.more_info{
		font-size: 15px; 
		color: var(--fadeColor);
		font-weight: 500;
	}
	.ant-typography{
    margin : 0;
		font-weight: bold;
		font-size: 17px;
  }
`
const HeaderMenu = () => {
	const user = useSelector((state : RootState) => state.user);
	const theme = useSelector((state : RootState) => state.theme.value);
	return (
		<HeaderContainer style={{display : "flex" , justifyContent : "space-between" , alignItems : "center"}}>
			<div className="infoPart" style={{alignItems : "center"}}>
				<Avatar style={{  margin : "0 8px" , width : 40 , height : 40}} src={user.avatar} />
				<div>
					<Title style={theme ? {color : "var(--light)"} : {color : "var(--dark)"}} level={5}>{user.fullName}</Title>
					<p className="more_info">Frontend Developer</p>
				</div>

			</div>
			<span>
				<SettingOutlined style={{fontSize : 20}}/>
			</span>
		</HeaderContainer>
	)
}

export default HeaderMenu;