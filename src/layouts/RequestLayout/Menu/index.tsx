import {Card, Divider} from "antd";
import HeaderMenu from "@/layouts/RequestLayout/Menu/headerMenu";
import styled from "styled-components";
import MenuContent from "@/layouts/RequestLayout/Menu/MenuContent";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import {memo} from "react";
const MenuContainer = styled.div`
	.ant-divider{
		margin: 10px 0;
		
	}
`
const Menu = () => {
	const theme = useSelector((state : RootState) => state.theme.value);
	return (
		<MenuContainer>
			<Card style={theme ? {backgroundColor : "var(--darkBackground) " , color : "var(--light)" , border : "none"} : {backgroundColor : "var(--light) " , color : "var(--dark)" }}>
				<HeaderMenu />
				<Divider style={theme ? {color : "white"} : {color : "var(--fadeColor)"}} />
				<MenuContent />
			</Card>
		</MenuContainer>
	)
}
export default memo(Menu);