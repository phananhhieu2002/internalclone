import styled from "styled-components";
import ItemCard from "@/components/share-components/ItemCard";
import editImg from "@/public/assets/editSVG.svg";
import DocumentImg from "@/public/assets/documentSVG.svg";
import arrowImg from "@/public/assets/arrowSVG.svg";
const MenuContainer = styled.div`

`
const MenuContent = () => {
	return (
		<MenuContainer>
			<a href="#">
				<ItemCard title="Soạn yêu cầu" source={editImg}  des="Tạo yêu cầu của bạn"/>
			</a>
			<a href="#">
				<ItemCard title="Yêu cầu đã nhận" source={DocumentImg}  des="Quản lý danh sách các yêu cầu"/>
			</a>
			<a href="#">
				<ItemCard title="Yêu cầu đã gửi" source={arrowImg}  des="Xem các yêu cầu đã gửi"/>
			</a>
		</MenuContainer>
	)
}

export default  MenuContent;