import {Avatar, Card, Col} from "antd";
import { Typography } from 'antd';
import Image from "next/image";
import styled from "styled-components";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";
const { Title } = Typography;
const ItemWrapper = styled.div`
	.ant-typography{
		font-size: 16px;
	}
	.ant-card-body{
		text-align: center;
	}
	.image{
		margin: 10px auto;
		border-radius: 100%;
	}
	.column:hover{
		cursor: pointer !important;
	}
`
interface itemProp{
	image : any ,
	title : string
}
const Item = ({image , title} : itemProp) => {
	const theme = useSelector((state : RootState) => state.theme.value)
	return (
			<Col xs={24} md={12} lg={6} style={{padding : "0 8px"}}>
				<ItemWrapper>
					<Card style={theme ? {backgroundColor : "var(--darkBackground)" , border : "none"} : {backgroundColor : "var(--light)"}}>
						<Image className="image"  src={image} />
						<Title style={theme ? {color : "var(--light)"} : {color : "var(--dark)"}} level={4}>{title}</Title>
					</Card>
				</ItemWrapper>
			</Col>
	)
}

export default  Item ;