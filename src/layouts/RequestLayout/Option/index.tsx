import {Row} from "antd";
import Item from "@/layouts/RequestLayout/Option/Item";
import svg1 from "@/public/assets/svg1.svg";
import svg2 from "@/public/assets/svg2.svg";
import svg3 from "@/public/assets/svg3.svg";
import svg4 from "@/public/assets/svg4.svg";
import styled from "styled-components";
import {memo} from "react";
const OptionContainer = styled.div`
	
	.ant-col:hover{
    cursor: pointer;
		transition: all 1.5s;
		transform: translateY(-10px);
	}
`
const Options = () => {
	return (
		<OptionContainer>
			<Row style={{marginBottom : 20}}>
				<Item image={svg1} title="Đăng kí Overtime" />
				<Item image={svg2} title="Đăng kí lịch học" />
				<Item image={svg3} title="Xin nghỉ" />
				<Item image={svg4} title="Yêu cầu khác" />
			</Row>
		</OptionContainer>
	)
}

export default memo(Options);