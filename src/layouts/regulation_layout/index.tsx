import styled from "styled-components";
import {Input, Row, Table} from "antd";
import {SearchOutlined} from "@ant-design/icons";
import React from "react";
import {ColumnsType} from "antd/es/table";

const RegulationContainer = styled.div`
  .ant-row{
    justify-content: space-between;
    margin-bottom: 20px;
  }
  .option{
    display: flex;
  }
  .title{
    font-size: 19px;
    font-weight: 500;
  }
  .ant-input{
    max-height: 40px;
    max-width: 150px;
  }
  .search_hrm{
    position: relative;

  }
  .option *{
    margin-left: 10px;
  }
  .anticon-search{
    position: absolute;
    right: 10px;
    top: 29%;
  }
  .search_hrm input{
    min-width: 210px;
  }
  .ant-table-wrapper{
    width: 100%;
  }
`
// interface DataType{
//     title : string ,
//     dataIndex : string ,
//     key : string
// }
type DataType = {
    title : string ,
    dataIndex : string ,
    key : string
}
const columns: ColumnsType<DataType> = [
    {
        title: 'STT',
        dataIndex: 'stt',
        key: 'stt',
    },
    {
        title: 'Tiêu đề',
        dataIndex: 'title',
        key: 'title',
    },
    {
        title: 'Mức tiền',
        dataIndex: 'money',
        key: 'money',
    },
    {
        title: 'Nội dung',
        key: 'content',
        dataIndex: 'content',
    },
    {
        title: 'Hành động',
        key: 'action',
    },
];
interface  titleProp {
    title : string
}
const RegulationLayout = ({title} : titleProp) => {
    return (
        <RegulationContainer>
            <Row>
                <h1 className="title">{title}</h1>
                <div className="option">
                    <Input placeholder="$Từ" />
                    <Input placeholder="$Đến" />
                    <div className="search_hrm">
                        <Input placeholder="Tìm kiếm..." />
                        <SearchOutlined />
                    </div>
                </div>
            </Row>
            <Row>
                <Table columns={columns} />
            </Row>
        </RegulationContainer>
    )
}

export default RegulationLayout;