import styled from "styled-components";
import {Button, Menu} from "antd";
import React, {PropsWithChildren} from "react";
import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import {MenuProps} from "antd/es/menu";
import {open} from "@/redux/slices/app.slice";
import {propsType} from "@/@types/global";



const MenuLayoutContainer = styled.div`
  margin: 25px 0;
  padding: 0;
  display: flex;
  background-color: #fff;
  .side_content{
    margin-top: 9px;
  }
  .btn_create{
    background-color : #1677ff;
    box-sizing: content-box;
  }
  .create_box{
    padding: 16px;
    display: flex;
    justify-content: center;
  }
  .ant-menu-item{
    margin: 0;
    width: 100%;
    border-radius: 0;
    padding: 0 16px 0 24px;
    margin: 4px 0;
  }
  .ant-menu .ant-menu-item:hover{
    color: #1677ff !important;
    background-color: transparent !important;
  }
  .ant-menu-item-selected{
    border-right: 2px solid #1677ff;
  }
  .main_content{
    padding: 24px;
    width: calc(100% - 200px);
    margin-top: 4px;
  }
    .ant-menu-title-content{
        font-weight: 600 !important;
    }
`
interface Props{
    children : PropsWithChildren,
    items: propsType,
    icon?: any,
    text: string,
    keyProp : string
}
const MenuLayout = ({children, items, icon, text , keyProp}: Props) => {
    const dispatch = useDispatch();
    const router = useRouter();
    const handleClick : MenuProps['onClick'] = (e) => {
        router.replace(e.key)
        // Call API here
    }
    const createNew = () => {
        if(keyProp) {
            console.log(keyProp)
            router.replace(keyProp);
        }
        else{
            dispatch(open());
        }
    }
    return (
        <MenuLayoutContainer>
            <div className="side_content">
                <div className="side_content-main">
                    <div className="create_box">
                        <Button
                            type="primary"
                            className='btn_create'
                            icon={icon}
                            onClick={createNew}
                            style={{width : "100%"}}
                        >
                            {text}
                        </Button>
                    </div>
                    <div className="menu">
                        <Menu
                            style={{width: 200}}
                            items={items}
                            defaultSelectedKeys={[`${router.pathname}`]}
                            onClick={handleClick}
                        />
                    </div>
                </div>
            </div>
            <div className="main_content">
                {children}
            </div>
        </MenuLayoutContainer>
    )
}

export default MenuLayout;