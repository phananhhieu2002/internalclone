import styled from "styled-components";
import {Menu} from "antd";
import {InnerPropsType} from "@/@types/global";
import {useRouter} from "next/router";

const InnerLayout = styled.div`
  display: flex;
  .ant-menu-item-selected {
    border-right: 2px solid #3e79f7;
    transition: all 1s;
  }
  .ant-menu-item ,
  .ant-menu-submenu,
  .ant-menu-submenu-title{
    width: 100%;
    margin: 0;
    border-radius: 0;
  }
  .ant-menu-item:hover {
    color: #3e79f7 !important;
    background-color: transparent !important;
  }
  .ant-menu-submenu-title:hover{
    color: #3e79f7 !important;
    background-color: transparent !important;
  } 
  .side_content{
    width: 200px;
    height: 100%;
  }
  .main_content{
    width: 100%;
    margin-left: 20px;
  }
  .ant-menu-title-content{
    font-weight: 600 !important;
  }
`

const InnerAppLayout = ({children , items} : InnerPropsType) => {
  const router = useRouter();
  const handleClick = (e) => {
    router.push(`/documents/${e.key}`);
  }
  return (
    <InnerLayout>
      <Menu
        className="side_content"
        items={items}
        defaultSelectedKeys={['/']}
        onClick={handleClick}
      />
      <div className="main_content">
        {children}
      </div>
    </InnerLayout>
  )
}

export default InnerAppLayout;