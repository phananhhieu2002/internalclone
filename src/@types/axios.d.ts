type AxiosRequestConfig = import('axios').AxiosRequestConfig;
interface HttpRequestConfig extends AxiosRequestConfig {
  isNotRequiredAuthentication?: boolean;
}
