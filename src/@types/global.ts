import {PropsWithChildren} from "react";
import type { MenuProps } from 'antd';
import {Account} from "@/@types/auth";

export type PropType = {
  name?: any,
  amount?: string,
  process?: string,
  text?: string,
  title? : string,
  icon? : any,
  des? : string,
  source? : any,
  color? : any
}
export interface taskType {
  data : Task[],
  pagination : {
    total : number ,
    page? : number,
    hasPrev? : boolean,
    hasNext? : boolean
  },
  information : {
    name : string,
    description : string,
    members : Account[],
    status : any,
    deadline : any
  }
}
export type categoryType = {
  key : string,
  path : string,
  title : string,
  icon : any
  breadcrumb : boolean,
  submenu : any[]
}


type MenuItem = Required<MenuProps>['items'][number];

export function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

export type propsType = {
  items : MenuItem[]
}

export type InnerPropsType = {
  children : PropsWithChildren,
  items? : propsType
}
export interface Group {
  id?: any;
  creator? : any;
  name: string;
  slug: string;
  coverPicture?: string;
  avatar?: string;
  description?: string;
  // privacy: GroupPrivacy;
  memberCount?: number;
  postCount?: number;
}
export interface Queries {
  limit?: number;
  page?: number;
  sortBy?: string;
  department?: string;
  fullName?: string;
  end?: string;
  start?: string;
  range?: string;
  role?: string;
  slug?: string;
  placeholder?: string;
  pagination?: boolean;
  querySearch?: string;
}
export interface TaskMembers extends Account {
  task?: Task;
  _id: string;
  createdAt?: Date;
  updatedAt?: Date;
}
interface Attachment {
  fileName: string;
  _id?: any;
  creator: Account;
  task: Task;
  fileType: 'doc' | 'photo' | 'video';
  link: string;
  path: string;
  size: string;
  metadata?: any;
}

interface Project {
  _id?: string;
  id?: string;
  name?: string;
  slug?: string;
  type?: string;
  description?: string;
  image?: string;
  // members?: ProjectMember[];
  memberCount?: number;
  taskCount?: number;
  completedTaskCount?: number;
  gitLabData?: {
    projectId: string;
    projectName: string;
    branch: string;
    webUrl: string;
    sshUrl: string;
    httpUrl: string;
    createdAt: string;
    domain: string;
    environmentVariables: [
      {
        key: string;
        value: string;
      }
    ];
  };
  createdAt?: number;
  updatedAt?: number;
  dueDate?: string;
}
type TaskState = 'TODO' | 'IN_PROGRESS' | 'COMPLETED' | 'ARCHIVED';
export interface Task {
  id: string;
  isDue?: boolean;
  _id?: string;
  creator?: Account;
  members?: TaskMembers[];
  state?: TaskState;
  name?: string;
  description?: string;
  startDate?: Date;
  dueDate?: Date;
  attachments?: Attachment[];
  createdAt?: Date;
  updatedAt?: Date;
  project?: Project;
}
export interface TaskQueries extends Queries {
  state?: string;
  sortBy?: string;
}
interface Pagination {
  total: number;
  page: number;
  hasNext: boolean;
  hasPrev: boolean;
}
export interface GetDataRes<T> {
  data : T ;
  pagination : Pagination ;
}
export interface TaskFilter {
  project?: string;
  owner?: string;
  creationStart?: number;
  creationEnd?: number;
  startTime?: number;
  endTime?: number;
  sortBy?: string;
  assigned?: string;
  limit?: number;
}
export interface Task{
  id: string;
  isDue?: boolean;
  _id: string;
  creator: Account;
  members: TaskMembers[];
  state: TaskState;
  name: string;
  description: string;
  startDate: Date;
  dueDate: Date;
  attachments: Attachment[];
  createdAt: Date;
  updatedAt?: Date;
  project?: Project;
}
export type TokenType = {
  access_token?: {
    token: string;
    expires: number;
  } | null;
  refresh_token?: { token: string; expires: number } | null;
};