interface AuthToken {
  access_token?: string;
  refresh_token?: string;
  access_token_expires_in?: number;
  access_token_expires_at?: number;
}
interface AuthConfig {
  headers: {
    Referer: string;
  }
}

export interface Account {
  activeEmail?: string;
  _id?: string;
  id?: string;
  avatar?: string;
  fullName?: string;
  username?: string;
  email?: string;
  address?: string;
  phone?: string;
  password?: string;
  role?: string;
  accessToken?: string;
  rePassword?: string;
  status?: string;
  joinedDate?: number;
  leavedDate?: number;
  gender?: string;
  dob?: number;
  entranceCards?: string[];
  isVerified?: boolean;
  createdAt?: number;
  updatedAt?: number;
  isTelegramConnected?: boolean;
  emails?: string[];
  identityNumber?: number;
  telegramId?: string;
  gitLabAccessToken?: string;
  stars?: number;
  exp?: number;
  balance?: number;
  gitLabId?: string | null;
  wakaId?: string;
  // workType?: WorkType;
  // workModel?: WorkModel;
  // department?: DepartmentModel;
  // jobInfo?: JobInfo;
}