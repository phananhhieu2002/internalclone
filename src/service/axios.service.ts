import axios from "axios";
import {API_BASE_URL, APP_TOKEN_KEY} from "@/configs/app.config";
import {getData, removeData, setData} from "@/service/storage.service";
import {TokenType} from "@/@types/global";

const handleRefreshToken = async (refreshToken: string): Promise<TokenType | null> => {
	// eslint-disable-next-line no-async-promise-executor
	return new Promise(async resolve => {
		try {
			const res = await axios.post(API_BASE_URL + '/auth/token', {
				refresh_token: refreshToken,
				grant_type: 'refresh_token',
			});
			console.log('==> Refresh token success');
			resolve(res.data);
		} catch (err) {
			console.log('==> Refresh token failed ' + err);
			resolve(null);
		}
	});
};

const axiosService = axios.create({
	timeout: 15000,
	baseURL: API_BASE_URL,
});

axiosService.interceptors.request.use(
	async (config) => {
		// check token
		let _config = config;
		const app_token = await getData<TokenType | null>(APP_TOKEN_KEY, null);
		if (!app_token) return _config;
		const {access_token, refresh_token, access_token_expires_in} = app_token;
		const currentTime = new Date().getTime();
		if (access_token && access_token_expires_in > currentTime) {
			return {
				..._config,
				headers: {
					..._config.headers,
					Authorization: `Bearer ${access_token}`,
				},
			};
		}
		if (refresh_token?.token) {
			// handle refresh_token token
			console.log('expires');
			try {
				const token = await handleRefreshToken(refresh_token.token);
				if (token) {
					await setData(APP_TOKEN_KEY, {
						access_token: token.access_token,
						refresh_token: token.refresh_token,
					});
					_config = {
						..._config,
						headers: {
							..._config.headers,
							Authorization: `Bearer ${token.access_token ?? ''}`,
						},
					};
				} else throw new Error('');
			} catch (err) {
				removeData('all');
				return (window.location.href = '/auth');
			}
		} else {
			removeData('all');
			window.location.href = '/auth';
		}
		return _config;
	},
	err => {
		return Promise.reject(err);
	}
);

export default axiosService;