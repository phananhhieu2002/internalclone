import axios, { Method } from "axios";
import axiosService from "@/service/axios.service";
import { GetDataRes, Group, Queries, Task, TaskQueries } from "@/@types/global";
import { Account } from "@/@types/auth";

export const dataQuery = (data: { [key: string]: string | number }) => {
	if (!data && Object.keys(data).length! > 0) return '';
	return Object.keys(data).reduce((result, x) => {
		return (result += `${x}=${data[x]}&`);
	}, '?');
};
export const request = <T>(endpoint: string, method: Method, data: any = {}, isFormData?: boolean): Promise<T> => {
	return new Promise((resolve, reject) => {
		const url = method.toUpperCase() === 'GET' ? endpoint + dataQuery(data) : endpoint;
		const _data = method.toUpperCase() === 'GET' ? {} : data;
		const headers = isFormData ? { 'Content-Type': 'multipart/form-data' } : { 'Content-Type': 'application/json' };
		axiosService
			.request({
				method,
				url,
				data: _data,
				headers,
			})
			.then(res => resolve(res.data))
			.catch(err => reject(err));
	});
};

class ApiService {
	// PRODUCTION API
	static appId = 'f69c2008-02ba-42ca-8445-4c2c139f45db';

	// DEVELOPMENT API
	// static appId = "ddbd8c60-cf0f-466b-91c9-030ff6a92c6e";

	// // PRODUCTION API
	static authServer = 'https://auth.northstudio.vn';

	// // DEVELOPMENT API
	// static authServer = "https://ns-auth-server.herokuapp.com";

	// TODO
	// static redirectUri = window.document.location.origin + '/auth/callback';
	static redirectUri = 'http://localhost:3000/internal/auth/callback';


	/* Account */
	static getSelfAccount() {
		return request<{ data: Account }>(`/accounts/me`, 'get', {});
	}

	/* User */
	static searchUser(data: Queries) {
		return request<GetDataRes<Account[]>>('/accounts/search', 'get', data);
	}


	/* Quote */
	static getRandomQuote() {
		return request<any>('/quote', 'get', {});
	}



	/* Task */
	static getGeneralTasks(queries: TaskQueries) {
		return request<GetDataRes<Task[]>>('/general-tasks', 'get', queries);
	}

	static deleteTask(id: string) {
		return request<{ data: Task }>(`/general-tasks/${id}`, 'delete', {});
	}

	static createGeneralTask(taskInfo: any) {
		return request<{ data: Task }>('/general-tasks', 'post', taskInfo, true);
	}
	static updateGeneralTask(id: string, taskInfo: any) {
		return request<{ data: Task }>(`/general-tasks/${id}`, 'post', taskInfo, true);
	}



	
	/* Group */
	static getGroups(queries: Queries) {
		return request<GetDataRes<Group[]>>('/groups', 'get', queries);
	}

	static deleteGroup(groupId: string) {
		return request<{ data: Group }>('/groups/' + groupId, 'delete', {});
	}

	static createGroup(data: any) {
		return request<{ data: Group }>('/groups', 'post', data, true);
	}

	static getGroup(groupId: string) {
		return request<{ data: Group }>('/groups/' + groupId, 'get', {});
	}
}

export default ApiService;