import { NextPageWithLayout } from "@/pages/_app";
import styled from 'styled-components';
import AppLayOut from "@/layouts/App_Layout";
import { Row, Col, Card, Tooltip, Button, Input, Modal, Form, Avatar, Upload, Typography, message } from "antd";
import CardComponent from "@/components/groups/Card";
import CardCommonComponent from "@/components/share-components/Card";
import { SearchOutlined, FilterOutlined, LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { useState } from "react";
import Head from "next/head";
import JoinedGroup from "@/components/groups/joinedGroup";
import { RcFile } from "antd/es/upload";
import type { RcFile, UploadFile, UploadProps } from 'antd/es/upload/interface';
import ApiService from "@/service/ApiService";
import Utils from "@/utils";
const GroupContainer = styled.div`

    .ant-row{
      margin-left: -8px;
      margin-right: -8px;
      row-gap: 0px;
    }
    .main{
        padding: 0 8px;
    }
  .main .anticon{
    color: rgb(24, 144, 255);
  }
  .header{
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  :where(.css-dev-only-do-not-override-12jzuas).ant-btn-primary{
    background-color: #1677ff !important;
  }
  .control{
    display: flex;
  }
  .control button{
    margin: 0 5px;
  }
  .header .other h5{
    font-size: 22px;
    font-weight: 400;
  }
      .card_info{
    height: 200px;
    margin-top: 10px;
    border-radius: 10px;
    border: 1px solid rgb(230,235,241);
    padding: 25px;
  }
    .card_info p{
        color: #455560;
        font-weight: 500;
        font-size: 15px;
    }
  .card_info h5{
    font-size: 25px;
      font-weight: 500;
    margin-bottom: 10px;
  }
    
  .gropup_In{
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 24px;
  }
  .ant-input-affix-wrapper{
    max-width: 250px;
    max-height: 40px;
    padding: 7px;
    border-radius: 10px;
  }
  .gropup_In .part{
    font-size: 20px;
      font-weight: 600;
  }
  .ant-col-lg-8{
    padding: 0 8px;
  }
  .group_header{
    display: flex;
  }
  .group_header_logo{
    display: flex;
    margin-right: 10px;
    align-items: center;
  }
  .ant-avatar-circle{
    padding: 2px;
    box-sizing: content-box;
    background-color: #f0f2f5;
  }
  .other span{
      font-weight: 500;
    color: rgba(114,132,154,.7);
  }
  .ant-btn-circle , .ant-avatar-circle{
    display: flex;
    align-items: center;
    justify-content: center;
    
  }
  .ant-avatar-circle img{
    width: 25px;
    height: 25px;
  }
    .ant-card{
      //box-shadow: 0 0 2px #ccc;
        padding: 0 8px;
    }
  .ant-card-body{
    padding: 30px;
  }
  .card_info{
    margin-top: 24px;
  }
  .ant-col , .ant-card{
    border: none;
  }
  .ant-upload-wrapper{
    display: flex;
    margin-bottom: 10px;
    justify-content: center;
  }
  .ant-upload-wrapper{
    display: flex !important; 
    justify-content: center;
  }
`

const GroupPage: NextPageWithLayout = () => {
  const [groupName, setGroupName] = useState<string>("Tên nhóm");
  const [description, setDescription] = useState<string>("Mô tả về nhóm");
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState<string>("");
  const [isModalOpen, setModalOpen] = useState(false);
  const setGroup = () => {
    setModalOpen(true);
  }
  const cancelModal = () => {
    setModalOpen(false)
  }
  const setSucess = async () => {
    try {
      if(groupName.trim() === '')  return message.error("Vui lòng nhập tên nhóm");
      const formData = new FormData();
      formData.append('name' , groupName.trim());
      description.trim().length > 0 && formData.append('description' , description.trim());
      const res = await ApiService.createGroup(formData);
      setModalOpen(false)
    }
    catch (err) {
      console.log("Create groups Failure");
      Utils.showNotification("error" , "Tạo nhóm thất bại , vui lòng tạo lại");
    }
  }
  const getBase64 = (img: RcFile, callback: (url: string) => void) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result as string));
    reader.readAsDataURL(img);
  };
  const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile> , data : any) => {
    if (info.file.status === 'uploading') {
      setLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      getBase64(info.file.originFileObj as RcFile, (url) => {
        setLoading(false);
        setImageUrl(url);
      });
    }
  };
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );
  return (
    <GroupContainer>
      <Head>
        <title>Group</title>
      </Head>
      <Row>
        <Col xs={24} lg={16}>
          <div className="main">
            <Card>
              <div className="header">
                <div className="group_header">
                  <div className="group_header_logo">
                    <Avatar
                      src="https://www.seekpng.com/png/detail/215-2156215_diversity-people-in-group-icon.png"
                    />
                  </div>
                  <div className="group_header_info">
                    <div className="other">
                      <h5 className="group_header_title">Nhóm</h5>
                      <span>30 nhóm</span>
                    </div>
                  </div>
                </div>
                <div className="control">
                  <Tooltip title="Search">
                    <Button shape="circle" icon={<SearchOutlined />} />
                  </Tooltip>
                  <Tooltip title="Save">
                    <Button shape="circle" icon={<FilterOutlined />} />
                  </Tooltip>
                  <Button onClick={setGroup} type="primary">Tạo nhóm</Button>
                  <Modal onOk={setSucess} onCancel={cancelModal} title="Tạo lập nhóm" open={isModalOpen}>
                    <Form
                      style = {
                      {
                        display : "flex",
                        flexDirection : "column",
                        alignItems : "center"
                      }
                      }
                    >
                      <div>
                        <Upload
                          name="avatar"
                          listType="picture-circle"
                          className="avatar-uploader"
                          showUploadList={false}
                          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                          onChange={handleChange}
                        >
                          {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                        </Upload>
                      </div>
                      <Typography.Title
                        className="text-white mt-2"
                        style={{ margin: "10px 0"}}
                        level={5}
                        editable={{
                          onChange: value => {
                            setGroupName(value);
                          },
                        }}
                      >
                        {groupName}
                      </Typography.Title>
                      <Typography.Paragraph
                        ellipsis={{
                          rows: 3,
                        }}
                        className="text-white mt-2 text-center"
                        style={{ margin: "10px 0" }}
                        editable={{
                          onChange: value => {
                            setDescription(value);
                          },
                        }}
                      >
                        {description}
                      </Typography.Paragraph>
                    </Form>
                  </Modal>
                </div>
              </div>
              <div className="card_info">
                <h5>Tính năng nhóm</h5>
                <p>Tham gia các nhóm trên hệ thống hoặc tự tạo nhóm của bản thân bạn!</p>
              </div>
              <div className="joined_group">
                <JoinedGroup />
              </div>
            </Card>
          </div>
        </Col>
        <Col xs={24} lg={8}>
          <CardComponent />
          <CardCommonComponent title="Bài viết gần đây" />
        </Col>
      </Row>
    </GroupContainer>
  )
}

GroupPage.getLayout = function (page) {
  return (
    <AppLayOut>
      {page}
    </AppLayOut>
  )
}

export default GroupPage;