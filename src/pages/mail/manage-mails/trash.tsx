import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import MenuLayout from "@/layouts/Menu_Layout";
import {EditOutlined} from "@ant-design/icons";
import {Empty, Input, Row, Select} from "antd";
import Head from "next/head";
import {propsType} from "@/@types/global";
import {getItem} from "@/@types/global";
import {
    InboxOutlined ,
    MailOutlined ,
    FileTextOutlined,
    StarOutlined,
    DeleteOutlined
} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";

const items : propsType = [
    getItem('Hộp thư đến', '/mail/manage-mails/inbox' , <InboxOutlined />),
    getItem('Thư đã gửi', '/mail/manage-mails/sent' , <MailOutlined />),
    getItem('Bản thảo', '/mail/manage-mails/draft' , <FileTextOutlined />),
    getItem('Đã gắn sao', '/mail/manage-mails/starred' , <StarOutlined />),
    getItem('Đã xoá', '/mail/manage-mails/trash', <DeleteOutlined />),
]
const MailManage = styled.div`
  .toolbar_wrap{
    float: right;
  }
  .info{
    margin-top: 20px;
  }
  .toolbar{
    min-height: 100px;
  }
`

const Trash: NextPageWithLayout = () => {
  const user = useSelector((state : RootState) => state.user);

    return (
        <MailManage>
            <Head>
                <title>Mail</title>
            </Head>
            <div className="toolbar">
                <div className="toolbar_wrap">
                    <Row>
                        <Select
                            defaultValue={user.email}
                            style={{width: 200}}
                            options={[
                                {value: `${user.email}`, label: `${user.email}`},
                            ]}
                        />
                        <Input style={{width : 200 , margin : "0 10px"}} placeholder="Search"/>
                    </Row>
                </div>
            </div>
            <div className="info">
                <Empty />
            </div>
        </MailManage>
    )
}
Trash.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <MenuLayout items={items} text="Soạn tin" icon={<EditOutlined/>}  keyProp="/mail/manage-mails/compose">
                {page}
            </MenuLayout>
        </AppLayOut>
    )
}
export default Trash;