import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import MenuLayout from "@/layouts/Menu_Layout";
import {EditOutlined} from "@ant-design/icons";
import {Form, Modal, Select, Table} from "antd";
import {close} from "@/redux/slices/app.slice";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import InputComponent from "@/components/share-components/Input";
import Head from "next/head";
import {propsType} from "@/@types/global";
import {getItem} from "@/@types/global";
import {
    InboxOutlined ,
} from "@ant-design/icons";

const items : propsType = [
    getItem('Quản lý người nhận', '/mail/manage-receivers' , <InboxOutlined />),
]
interface DataType {
    key: string;
    name: string;
    age: number;
    address: string;
    tags: string[];
}

const columns: ColumnsType<DataType> = [
    {
        title: 'Tên danh sách',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Danh sách người nhận',
        dataIndex: 'list',
        key: 'list',
    },
    {
        title: 'Độ riêng tư',
        dataIndex: 'private',
        key: 'private',
    },
    {
        title: 'Hành động',
        dataIndex: 'action',
        key: 'action',
    },
];
const ReceiverManage = styled.div`
  .ant-select-selector{
    padding: 8px 11px;
  }
  .label{
    margin-bottom: 10px;
  }
`

const ManageReceiver : NextPageWithLayout = () => {
   const isModalOpen = useSelector((state : RootState) => state.app.modal);
   const dispatch = useDispatch();

    const handleOk = () => {
        dispatch(close());
    };

    const handleCancel = () => {
        dispatch(close());
    };
    return (
        <ReceiverManage>
            <Head>
                <title>Mail</title>
            </Head>
            <Modal title="Thêm danh sách" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Form
                    layout={"vertical"}
                    style={{padding : "24px 0"}}
                >
                    <InputComponent  label="Tên danh sách" placeholderText="" />
                    <InputComponent label="Danh sách người nhận" placeholderText="To:" />
                    <div className="selection">
                        <h3  className="label" style={{marginBottom : 10}}>Độ riêng tư</h3>
                        <Select
                            style={{width : "100%"}}
                            defaultValue="private"
                            options={[
                                { value: 'private', label: 'PRIVATE' },
                                { value: 'shared', label: 'SHARED' },
                                { value: 'public', label: 'PUBLIC' },
                            ]}
                        />
                    </div>
                    
                </Form>
            </Modal>
            <Table columns={columns} />
        </ReceiverManage>
    )
}
ManageReceiver.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <MenuLayout items={items} text="Thêm danh sách" icon={<EditOutlined />}>
                {page}
            </MenuLayout>
        </AppLayOut>
    )
}
export default ManageReceiver;
