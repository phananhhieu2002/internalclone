import AppLayOut from "@/layouts/App_Layout";
import { NextPageWithLayout } from "../_app";
import styled from 'styled-components';
import { Avatar, Button, Modal, Space, Spin, Table, Tag } from "antd";
import React, { ReactElement, useEffect, useState, useMemo } from "react";
import TaskLayout from "@/layouts/Task_Layout";
import Head from "next/head";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import * as TaskAct from "@/redux/actions/task";
import { RootState } from "@/redux/store";
import Utils from "@/utils";
import PaginationComponent from "@/components/share-components/Pagination";
import TaskForm from "@/components/tasks/TaskForm";
import { setIsModalOpen } from "@/redux/slices/app.slice";


const { Column } = Table;

interface User {
    name: string;
    avatar: string;
}

interface DataType {
    key: string;
    title: string;
    status: ReactElement;
    user?: User;
}


const MainContent = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: column;
    width: 100%;
  .ant-table {
    font-weight: 500 !important;
  }

  .ant-table-tbody {
    font-size: 13px !important;
  }

  .ant-btn {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .loading{
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .avatar_member{
    margin: 0 auto;
  }
`


const Index: NextPageWithLayout = () => {
    const isModalOpen = useSelector((state: RootState) => state.app.isModalOpen)
    const [id, setId] = useState<any>(null);
    const [total, setTotal] = useState(0);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(isModalOpen);
    const tasks = useSelector((state: RootState) => state.task);
    const page = useSelector((state : RootState) => state.app.page);
    const dispatch = useDispatch();
    const data = useMemo(() => {
        const result = tasks.data.map(task => {
            return {
                id: task.id,
                name: task.name,
                due: Utils.convertFormatDate(task.dueDate),
                status: Utils.parseStatus(task.state),
                member: task.creator && task.creator.avatar
            }
        })
        return result;
    }, [tasks]);
    const showModal = (item: any) => {
        setId(item.id);
        setOpen(true);
        dispatch(setIsModalOpen(true))
    };

    console.log(tasks);
    
    const handleCancel = () => {
        setOpen(false);
    };
    useEffect(() => {
        setLoading(true);
        dispatch(TaskAct.getTasks({ page : page, sortBy: '-createdAt' }));
        setTotal(tasks.pagination.total);
        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }, [page])
    useEffect(() => {
        setOpen(isModalOpen)
    }, [isModalOpen])
    return (
        <MainContent>
            <Head>
                <title>Task</title>
            </Head>
            {
                loading ?
                    <div className="loading">
                        <Spin size="large" />
                    </div>
                    :
                    <>
                        <Table pagination={false} dataSource={data}>
                            <Column title="Name" dataIndex="name" key="name" />
                            <Column title="Thời hạn" dataIndex="due" key="due" />
                            <Column
                                title="Trạng thái"
                                key="status"
                                render={(_: any, item: any) => (
                                    <Space size="middle">
                                        <Tag bordered={false} color={item.status.color}>
                                            {item.status.name}
                                        </Tag>
                                    </Space>
                                )}
                            />
                            <Column
                                title="Người tham gia"
                                key="member"
                                render={(_: any, item: any) => (
                                    <Space size="middle">
                                        <Avatar className="avatar_member" src={item.member} />
                                    </Space>
                                )}
                            />
                            <Column
                                title="Hành động"
                                key="action"
                                render={(item: any) => (
                                    <Space size="middle">
                                        <Button
                                            icon={<EditOutlined />}
                                            size={'middle'}
                                            onClick={() => {
                                                showModal(item)
                                            }}
                                        />
                                        <Button
                                            type="primary"
                                            danger
                                            icon={<DeleteOutlined />}
                                            onClick={() => {
                                                Modal.confirm({
                                                    title: "Bạn có chắc bạn muốn xóa task này không?",
                                                    onOk: async () => {
                                                        // const res = await ApiService.deleteTask(item.id);
                                                        dispatch(TaskAct.deleteTask(item.id))
                                                        Utils.showNotification('success', 'Xóa công việc thành công');
                                                        try {
                                                            // 
                                                        }
                                                        catch (err) {
                                                            console.log("==> delete request failed", err);
                                                            Utils.showNotification('error', 'Đã có lỗi xảy ra khi xóa công việc!');
                                                        }
                                                    }
                                                })
                                            }}
                                        >
                                        </Button>
                                    </Space>
                                )}
                            />
                        </Table>
                        <Modal
                            open={open}
                            onCancel={handleCancel}
                            style={{ minWidth: 1200 }}
                            footer={null}
                        >
                            <TaskForm title="Chỉnh sửa nhiệm vụ" id={id} />
                        </Modal>
                        <PaginationComponent total={total} item={tasks.data.length} current={page} />
                    </>
            }
        </MainContent>
    )
}
Index.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <TaskLayout>
                {page}
            </TaskLayout>
        </AppLayOut>
    )
}

export default Index;