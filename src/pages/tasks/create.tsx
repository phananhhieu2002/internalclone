import AppLayOut from "@/layouts/App_Layout";
import { NextPageWithLayout } from "../_app";
import styled from 'styled-components';
import { Row, Col, Select, Button, Menu, Table } from "antd";
import { CalendarOutlined, EditOutlined, ClockCircleOutlined, CheckCircleOutlined, CloseCircleOutlined, InboxOutlined } from '@ant-design/icons';
import type { MenuProps, MenuTheme } from 'antd/es/menu';
import type { ColumnsType } from 'antd/es/table';
import React, { ReactElement } from "react";
import TaskLayout from "@/layouts/Task_Layout";
import FormWrap from '../../components/tasks/TaskForm/index';

interface User {
    name: string;
    avatar: string;
}
interface DataType {
    key: string;
    title: string;
    status: ReactElement;
    user?: User;
}


const MainContent = styled.div`
    display: flex;
flex-direction: column;
    width: 100%;
`
type MenuItem = Required<MenuProps>['items'][number];


const Create: NextPageWithLayout = () => {
    return (
        <MainContent>
            <FormWrap title="Tạo nhiệm vụ" />
        </MainContent>
    )
}
Create.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <TaskLayout>
                {page}
            </TaskLayout>
        </AppLayOut>
    )
}

export default Create;