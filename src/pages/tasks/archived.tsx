import AppLayOut from "@/layouts/App_Layout";
import { NextPageWithLayout } from "../_app";
import styled from 'styled-components';
import { Row, Col, Select, Button, Menu, Table } from "antd";
import { CalendarOutlined, EditOutlined, ClockCircleOutlined, CheckCircleOutlined, CloseCircleOutlined, InboxOutlined } from '@ant-design/icons';
import type { MenuProps, MenuTheme } from 'antd/es/menu';
import type { ColumnsType } from 'antd/es/table';
import React, { ReactElement } from "react";
import TaskLayout from "@/layouts/Task_Layout";

interface User {
    name: string;
    avatar: string;
}
interface DataType {
    key: string;
    title: string;
    status: ReactElement;
    user?: User;
}
const columns: ColumnsType<DataType> = [
    {
        title: 'Tiêu đề',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Thời hạn',
        dataIndex: 'age',
        key: 'age',
    },
    {
        title: 'Trạng thái',
        dataIndex: 'address',
        key: 'address',
    },
    {
        title: 'Người tham gia',
        key: 'tags',
        dataIndex: 'tags',
    },
    {
        title: 'Hành động',
        key: 'action',
    },
];
const data: DataType[] = [
    {
        key: '3',
        firstName: 'Joe',
        lastName: 'Black',
        age: 32,
        address: 'Sydney No. 1 Lake Park',
        tags: ['cool', 'teacher'],
    },
];
const MainContent = styled.div`
    display: flex;
flex-direction: column;
    width: 100%;
`
type MenuItem = Required<MenuProps>['items'][number];


const Archived: NextPageWithLayout = () => {
    return (
        <MainContent>
            <Table columns={columns} dataSource={data} />
        </MainContent>
    )
}
Archived.getLayout = function getLayout(page) {
    return (
        <AppLayOut>
            <TaskLayout>
                {page}
            </TaskLayout>
        </AppLayOut>
    )
}

export default Archived;