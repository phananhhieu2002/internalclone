import styled from "styled-components";
import Head from "next/head";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import InnerAppLayout from "@/layouts/inner_app_layout";
import {
  FileTextOutlined,
  DeleteOutlined,
  FolderOutlined
} from "@ant-design/icons";
import React , {useState} from "react";
import {MenuProps, Col, Row, Button, Upload, Modal, Input, Form} from "antd";
import {props} from "@/configs/upload.config";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";

const Documents = styled.div`
  .heading {
    font-size: 25px;
  }

  .line-1 {
    display: flex;
    justify-content: space-between;
    padding: 30px 0;
    border-bottom: 1px solid #ccc;
  }
  
  .ant-btn-primary {
    background-color: #1677ff;
    display: flex;
    align-items: center;
    margin-right: 10px;
  }

  .ant-btn-default {
    display: flex;
    align-items: center;
  }

  .function {
    display: flex;
  }
  .ant-form-item-row{
    display: flex;
  }
`
type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Tất cả', '/', <FileTextOutlined/>),
  getItem('Thùng rác', '/trash', <DeleteOutlined/>),
];
const DocumentAll: NextPageWithLayout = () => {
  const [isModalOpen,setModelOpen] = useState(false);
  const openModel = () => {
    setModelOpen(true);
  }
  const handleCancel = () => {
    setModelOpen(false);
  }
  const handleOk = () => {
    setModelOpen(false);
  }
  // const members = useSelector((state : RootState) => state.member);
  // console.log(members)
  return (
    <Documents>
      <Head>
        <title>Documents</title>
      </Head>
      <Col>
        <Row className="line-1">
          <h1 className="heading">Tài liệu</h1>
          <div className="function">
            <Button
              type="primary"
              icon={<FolderOutlined/>}
              onClick = {openModel}
            >
              Thêm thư mục
            </Button>
            <Modal
              title="Tạo thư mục mới"
              open={isModalOpen}
              onOk={handleOk}
              onCancel={handleCancel}
            >
              <Form
                layout="vertical"
                initialValues={{
                  description: '',
                }}
              >
                <Form.Item
                  name="folder"
                  label="Tên thư mục"
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập tên thư mục!',
                    }
                  ]}
                >
                  <Input
                    placeholder={'Nhập tên thư mục'}
                  />
                </Form.Item>
              </Form>
            </Modal>
            <Upload {...props}>
              <Button icon={<FileTextOutlined/>}>Thêm tệp mới</Button>
            </Upload>
          </div>
        </Row>
        <Row></Row>
      </Col>
    </Documents>
  )
}

DocumentAll.getLayout = function getLayout(page) {
  return (
    <AppLayOut>
      <InnerAppLayout items={items}>
        {page}
      </InnerAppLayout>
    </AppLayOut>
  )
}
export default DocumentAll;
