import styled from "styled-components";
import Head from "next/head";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import InnerAppLayout from "@/layouts/inner_app_layout";
import React from "react";
import {MenuProps, Col , Row  } from "antd";
import {FileTextOutlined , DeleteOutlined} from "@ant-design/icons";

const Documents = styled.div`
  .heading{
    font-size: 25px;
  }
  .line-1{
    display: flex;
    justify-content: space-between;
    padding: 30px 0;
    border-bottom: 1px solid #ccc;
  }
`
type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key: React.Key,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}
const items : MenuItem[]  = [
  getItem('Tất cả', '/', <FileTextOutlined />),
  getItem('Thùng rác', '/trash', <DeleteOutlined />),
];
const DocumentAll: NextPageWithLayout = () => {
  return (
    <Documents>
      <Head>
        <title>Documents</title>
      </Head>
      <Col>
        <Row className="line-1">
          <h1 className="heading">Tài liệu</h1>
        </Row>
        <Row></Row>
      </Col>
    </Documents>
  )
}

DocumentAll.getLayout = function getLayout(page) {
  return (
    <AppLayOut>
      <InnerAppLayout items={items}>
        {page}
      </InnerAppLayout>
    </AppLayOut>
  )
}
export default DocumentAll;
