import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import styled from "styled-components";
import {
    Card,
    Col,
    Row,
    Spin,
    Empty,
    Dropdown,
    Modal,
    Calendar,
    Input,
    message,
    Form,
    DatePicker,
    Upload,
    UploadProps, UploadFile
} from "antd";
import { Typography } from 'antd';
import {EllipsisOutlined, LoadingOutlined, PlusOutlined, SearchOutlined} from "@ant-design/icons";
const { Title } = Typography;
import type { MenuProps } from 'antd';
import React, {useState} from "react";
import InputComponent from "@/components/share-components/Input";
import TextArea from "@/components/share-components/TextArea";
import Head from "next/head";
import {RcFile, UploadChangeParam} from "antd/lib/upload";

const ProjectContainer = styled.div`
  .ant-card-body{
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 15px;
  }
  .ant-typography{
    margin: 0;
  }
  .ant-card-body:before , .ant-card-body:after{
    display: none;
  }
  .anticon-ellipsis{
    font-size: 17px;
  }
  .project_list{
    min-height: 500px;
    margin-top: 20px;
  }
  .ant-col{
    padding: 0 10px;
  }
  .ant-picker-calendar{
    margin: 20px 0;
  }
  .ant-typography{
    font-weight: 400;
  }
  .ant-input-affix-wrapper{
    padding: 8px 11px;
    border-radius: 8px;
    border: none;
    margin-bottom: 20px;
  }
  .ant-modal-body{
    padding: 24px;
  }
  .ant-form-item{
    width: 100%;
  }
  .deadline p{
    padding-bottom: 8px;
  }
  .ant-upload-wrapper{
    display: flex;
    justify-content: center;
  }
    .ant-typography{
        font-weight: 500;
    }
`

const Project : NextPageWithLayout = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [status,setStatus] = useState("none")
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState<string>();
    const handleClick = (e : any) => {
        e.key === "0" ? showModal() : setStatus("flex");
    }
    const items: MenuProps['items'] = [
        {
            label: "Tạo project",
            icon : <PlusOutlined />,
            key: '0',
            onClick : handleClick
        },
        {
            label: "Tìm kiếm",
            icon : <SearchOutlined />,
            key: '1',
            onClick : handleClick
        },
    ];
    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };
    const handleChange: UploadProps['onChange'] = (info: UploadChangeParam<UploadFile>) => {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj as RcFile, (url) => {
                setLoading(false);
                setImageUrl(url);
            });
        }
    };
    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );
    return (
        <ProjectContainer>
            <Head>
                <title>Project</title>
            </Head>
            <Row>
                <Col xs={24} lg={8}>
                    <Spin
                        spinning={false}
                    >
                        <div className="header_card">
                            <Card>
                                <Title level={4}>Dự án đang tham gia</Title>
                                <Dropdown menu={{items}} trigger={['click']} >
                                    <EllipsisOutlined  />
                                </Dropdown>
                                <Modal title="Tạo dự án" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                                    <Form
                                        layout={"vertical"}
                                    >
                                            <Form.Item>
                                                <Upload
                                                    name="avatar"
                                                    listType="picture-circle"
                                                    className="avatar-uploader"
                                                    showUploadList={false}
                                                    action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                                    // beforeUpload={beforeUpload}
                                                    onChange={handleChange}
                                                >
                                                    {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
                                                </Upload>
                                            </Form.Item>
                                            <Form.Item style={{margin : 0}}>
                                                <Row className="line_2" style={{justifyContent : "space-between"}}>
                                                    <Col xs={24} md={12} style={{padding : "0px 8px"}}>
                                                        <InputComponent label="Tên dự án" placeholderText="Nhập tên dự án..." />
                                                    </Col>
                                                    <Col xs={24} md={12} style={{padding : "0px 8px"}}>
                                                        <div className="deadline">
                                                            <p style={{paddingBottom : 8}}>Hạn hoàn thành</p>
                                                            <DatePicker style={{width : "100%"}} />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Form.Item>
                                            <Form.Item>
                                                <InputComponent label="Mô tả dự án" placeholderText="Mô tả dự án..." />
                                            </Form.Item>
                                    </Form>
                                </Modal>
                            </Card>
                        </div>
                        <div className="project_list">
                            <Input style={{display : `${status}`}} size="small" placeholder="Nhập tên dự án" prefix={<SearchOutlined />} />
                            <Empty />
                        </div>
                    </Spin>
                </Col>
                <Col xs={24} lg={16}>
                    <Card>
                        <Spin
                            spinning={false}
                        >
                            <div className="timeline">
                                <Title level={4}>Nhiệm vụ của bạn</Title>
                                <Calendar />
                            </div>
                        </Spin>
                    </Card>
                </Col>
            </Row>
        </ProjectContainer>
    )
}

Project.getLayout = function getLayout(page ) {
    return (
        <AppLayOut>
            {page}
        </AppLayOut>
    )
}

export default Project;