import '@/styles/globals.scss'
import type {AppProps} from 'next/app'
import {NextPage} from "next";
import {ReactElement, ReactNode} from "react";
import {Router} from "next/router";
import nProgress from 'nprogress';
import {store} from "@/redux/store";
// import {PersistGate} from "redux-persist/integration/react";
import {Provider} from "react-redux";
import {BrowserRouter} from "react-router-dom";

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode
}
interface MyAppProps extends AppProps {
  Component: NextPageWithLayout;
  pageProps: any;
}

export default function MyApp(props: MyAppProps) {
  const {Component, pageProps} = props;
  const getLayout = Component.getLayout ?? ((page) => page);

  Router.events.on('routeChangeStart', nProgress.start);
  Router.events.on('routeChangeError', nProgress.done);
  Router.events.on('routeChangeComplete', nProgress.done);

  nProgress.configure({showSpinner: false});

  return (
    <Provider store={store}>
      {/*<BrowserRouter>*/}
        {getLayout(<Component {...pageProps} />)}
      {/*</BrowserRouter>*/}
    </Provider>
  )
}
