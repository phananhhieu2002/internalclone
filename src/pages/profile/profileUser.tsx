import styled from 'styled-components';
import { NextPageWithLayout } from '../_app';
import AppLayOut from '@/layouts/App_Layout';
import { Row, Col, Card } from 'antd';
import { HomeOutlined, MailOutlined, CalendarOutlined, PhoneOutlined } from '@ant-design/icons';

const Container = styled.div`
        margin: 200px 150px;
        .line-1{
            width:  100%;
            height: 170px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 10px;
        }
        .avatar{
            width: 150px;
            height: 150px;
            background-color: #ccc;
            border-radius: 11px;
            border: none !important;
            /* position: absolute; */
            top: -20px;
            left: 40px;
        }
        .ant-card{
            width: 100%;
        }
        .ant-card-body{
            padding: 8px 8px 24px 8px;
            display: flex;
            width: 100%;
        }
        .card-info{
            position: relative;
            display: flex;
        }

`
const profileUser: NextPageWithLayout = () => {
    return (
        <Container className="container">
            <Row className='line-1'>
                <Card
                    className='card-info'
                >
                    <div className="avatar"></div>
                    <Row className='infomation'>
                        <Row>
                            <Col>
                                Nguyễn Anh Nhân
                            </Col>
                        </Row>
                        <Row>
                            <Row>
                                <Col>
                                    <MailOutlined />
                                    Email:

                                </Col>
                                <Col>
                                    <CalendarOutlined />
                                    Ngày sinh:
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <PhoneOutlined />
                                    Điện thoại:

                                </Col>
                                <Col>
                                    <HomeOutlined />
                                    Địa chỉ:
                                </Col>
                            </Row>
                        </Row>
                    </Row>
                </Card>
            </Row>
            <Row className='line-2'>

            </Row>
        </Container>
    )
}
profileUser.getLayout = function getLayout(page) {
    return (
        <AppLayOut>{page}</AppLayOut>
    )
}
export default profileUser;