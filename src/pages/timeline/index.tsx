import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import styled from "styled-components";
import Head from "next/head";
import {Calendar, Form, Modal} from 'antd';
import type { Dayjs } from 'dayjs';
import type { CalendarMode } from 'antd/es/calendar/generateCalendar';
import React, {useState} from "react";
import FormItem from "@/components/share-components/FormItem";

const CalendarStyled = styled.div`
    .ant-modal-footer .ant-btn-primary{
      background-color: #1677ff !important;
    }
  .ant-form-vertical{
    overflow: scroll;
    max-height: 600px;
  }
    .ant-form-item-label{
        min-width: 150px !important;
    }
`
const TimeLine : NextPageWithLayout = () => {
    const [isModalOpen , setModalOpen] = useState(false)
    const onPanelChange = (value: Dayjs, mode: CalendarMode) => {
        console.log(value.format('YYYY-MM-DD'), mode);
    };
    const setEvent = (date : Dayjs) => {
        setModalOpen(true);
    }
    const cancelModal = () => {
        setModalOpen(false);
    }
    const createSuccess = () => {
        setModalOpen(true);
    }
    return (
        <CalendarStyled>
            <Head>
                <title>Timeline</title>
            </Head>
            <Calendar onSelect={setEvent} onPanelChange={onPanelChange} />
            <Modal
                open={isModalOpen}
                title="Tạo sự kiện mới"
                onCancel={cancelModal}
                onOk={createSuccess}
                style={{borderBottom : "1px solid #ccc"}}
            >
                <Form
                    layout="vertical"
                    initialValues={{
                        description: '',
                    }}
                    style={{paddingTop : 24 , paddingBottom : 24 , overflowY : 'scroll'}}
                >
                    <FormItem name="tên sự kiện" label="Tên sự kiện " type="input" ></FormItem>
                    <FormItem name="mô tả dự án" label="Mô tả " type="textarea" ></FormItem>
                    <FormItem name="sự kiện" label="Loại sự kiện " type="select" options={[
                        {value: 'enterprise', label: 'Sự kiện công ty'},
                        {value: 'personal', label: 'Sự kiện cá nhân'},
                        {value: 'holidey', label: 'Ngày nghỉ lễ'},
                    ]} ></FormItem>
                    <FormItem name="Chế độ" label="Chọn chế độ" type="checkbox" ></FormItem>
                    <FormItem name="địa điểm" label="Địa điểm " type="input" ></FormItem>
                    <FormItem name="độ riêng tư" label="Độ riêng tư " type="select" options={[
                        {value: 'private', label: 'Private'},
                        {value: 'public', label: 'Public'},
                    ]} ></FormItem>
                </Form>
            </Modal>
        </CalendarStyled>
    )
}
TimeLine.getLayout = function getLayout(page){
    return (
        <AppLayOut>
            {page}
        </AppLayOut>
    )
}
export default  TimeLine;