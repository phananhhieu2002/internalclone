import {Button, Col, Row} from "antd";
import styled from "styled-components";
import Head from "next/head";
import React from "react";
import {authService} from "@/service/auth.service";
import  {setData} from "@/service/storage.service";
import {AUTH_KEYS} from "@/configs/keys.config";
import {useRouter} from "next/router";

const {TOKEN, ACCESS_TOKEN, REFRESH_TOKEN, ACCESS_TOKEN_EXPIRES_IN, ACCESS_TOKEN_EXPIRES_AT} = AUTH_KEYS;

const Page = styled.div`
    height: 100vh;
  .ant-row{
    height: 100%;
  }
  .container {
    height: 100%;
  }
  .introducer{
    height: 100%;
    position: relative;
  }
  .text-right{
    position: absolute;
    right: 20px;
  }
  .text-white{
      font-size: 13px;
    font-weight: 500;
  }
  .introducer_info{
    position: absolute;
    top: 15%;
    color: #fff !important;
  }
  .sub{
    position: absolute;
    bottom: 10px;
    right: 10px;
  }
  .mx-2{
    margin: 0 10px;
  }
  .text-white-heading{
    font-size: 30px;
      font-weight: 700 !important;
      margin-bottom: 15px;
      margin-top: 30px;
  }
  .policy{
    display: flex;
    color: #fff;
  }
  .heading{
    font-size: 30px;
    font-weight: 700;
  }
  .guide{
    margin: 15px 0;
  }
  .Mainner{
    display: flex;
    flex-direction: column;
    /* align-items: center; */
    justify-content: center;
  }
    .sign_up{
        font-weight: 600;
        color: #1677ff;
        margin-left: 6px;
    }
`
const backgroundURL = '/assets/img-17.jpg';
const backgroundStyle = {
    backgroundImage: `url(${backgroundURL})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
};
const InputPage = () => {
    const router = useRouter();
    const handleSubmit = (e : any) => {
        let loginDialog: any;
        const isCordova = !!window.cordova;
        if (isCordova) {
            loginDialog = window.cordova['InAppBrowser'].open(authService.generateAuthUrl());
        } else {
            loginDialog = window.open( authService.generateAuthUrl(), '_blank');
        }
        const authorizationInterval = setInterval(async () => {
            try {
                let search = '';
                if (isCordova) {
                    search = await new Promise(_ => {
                        loginDialog.executeScript(
                          {
                              code: `window.location.search`,
                          },
                          (values: string[]) => _(values[0])
                        );
                    });
                    console.log(search);
                } else {
                    search = loginDialog?.location.search;
                }
                if (search.includes('code=')) {
                    clearInterval(authorizationInterval);
                    const searchQuery = search
                      .replace('?', '')
                      .split('&')
                      .find(x => x.includes('code='));
                    if (searchQuery) {
                        const code = searchQuery.split('=')[1];
                        if (code) {
                            loginDialog.close();
                            const data = await authService.requestToken(code, "");
                            setData(TOKEN, {
                                [ACCESS_TOKEN]: data?.access?.token,
                                [REFRESH_TOKEN]: data?.refresh?.token,
                                [ACCESS_TOKEN_EXPIRES_IN]: data?.access?.expires
                            })
                            // window.location.href = '/home';
                            router.replace("/home");
                        } else {
                            throw new Error('Auth error: Invalid code.');
                        }
                    } else {
                        throw new Error('Auth error.');
                    }
                }
            } catch (e) {
                console.log(e);
                console.log('--> Sign In on progress');
            }
        }, 500);
    }

    return (
        <Page className="page">
            <Head>
                <title>Authentication</title>
            </Head>
            <Row>
                <Col xs={20} sm={20} md={24} lg={16}>
                    <div className="container d-flex flex-column justify-content-center h-100">
                        <Row justify="center">
                            <Col className="Mainner" xs={24} sm={24} md={20} lg={12} xl={8}>
                                <h1 className="heading">Sign In</h1>
                                <p className='guide'>
                                    Don't have an account yet?
                                    <a href="https://northstudio.dev/auth/sign-up" className="sign_up" >Sign up</a>
                                </p>
                                <div className="mt-4">
                                    <Button onClick={handleSubmit} type="primary" htmlType="submit" block>
                                        Sign In with NorthStudio Account
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col xs={0} sm={0} md={0} lg={8}>
                    <div className="introducer d-flex flex-column justify-content-between h-100 px-4" style={backgroundStyle}>
                        <div className="text-right">
                            <img src="/assets/logo-white.png" alt="logo" />
                        </div>
                        <Row justify="center">
                            <Col className="introducer_info" xs={0} sm={0} md={0} lg={20}>
                                <img className="img-fluid mb-5" src="/assets/img-18.png" alt="" />
                                <h1 className="text-white-heading">Welcome to NorthStudio</h1>
                                <p className="text-white">
                                    This is the company's system with powerful management features and great people make it up.
                                </p>
                            </Col>
                        </Row>
                        <div className="sub d-flex justify-content-end pb-4">
                            <div className="policy">
                                <div>
                                    Term & Conditions
                                </div>
                                <span className="mx-2 text-white"> | </span>
                                <div>
                                    Privacy & Policy
                                </div>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </Page>
    )
}
export default InputPage;