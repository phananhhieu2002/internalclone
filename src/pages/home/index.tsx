import CardTypo from "@/components/share-components/CardTypo/CardTypo";
import Task from "@/components/share-components/Task/Task";
import { Card, Col, Row } from "antd";
import AppLayOut from "@/layouts/App_Layout";
import { NextPageWithLayout } from "@/pages/_app";
import styled from 'styled-components';
import RankComponent from "src/components/home_component/rank";
import moment from 'moment';
import TemperatureComponent from "@/components/home_component/Temperature";
import Head from "next/head";
import {useEffect, useState} from "react";
import ApiService from "@/service/ApiService";
import {RootState} from "@/redux/store";
import {useSelector} from "react-redux";

const HomePage = styled.div`
  .ant-row{
    flex-wrap: nowrap;
  }
  .ant-col{
    //padding: 0 8px;
    //box-sizing: content-box;
  }
  .inside{
    padding: 0 !important;
    background-color: transparent !important;
  }
  .row-2{
    margin-top: 20px;
  }
  .row-2 .ant-col{
    padding-right: 8px;
  }
  .ant-card-extra a{
    color: rgb(62, 121, 247);
    font-size: 17px;
    font-weight: bold;
  }
  .item{
    padding: 0;
  }
  .history_exit{
    min-height: 270px;
  }
`


const Home: NextPageWithLayout = () => {
    const tasks = useSelector((state : RootState) => state.task);
    const [todo , setTodo] = useState<string | number>(0);
    const [inProgress , setInProgress] = useState<string | number>(0);
    const [completed , setCompleted] = useState<string | number>(0);
    const [quote , setQuote] = useState('""""-');
    const currentDate = moment().format("MMM Do");
    useEffect(() => {
        ApiService.getRandomQuote()
          .then(res => {
              setQuote(`"${res[0].q}" - ${res[0].a}`);
          })
    },[])
    return (
        <HomePage className="inside">
            <Head>
                <title>Internal</title>
            </Head>
            <Row>
                <Col span={18}>
                    <Row className="row-1">
                        <Col className="item item_survive" xs={24} sm={24} md={24} lg={24} xl={8}>
                            <Task
                              name="Công việc tồn đọng"
                              amount={`${todo} tasks`}
                              process="Công việc chưa được xử lý"
                              color="rgb(255, 107, 114)"
                            />
                        </Col>
                        <Col className="item item_processing" xs={24} sm={24} md={24} lg={24} xl={8}>
                            <Task
                              name="Công việc đang tiến hành"
                              amount={`${inProgress} tasks`}
                              process="Công việc đang được xử lý"
                              color="rgb(250, 173, 20)"
                            />
                        </Col>
                        <Col className="item item_success" xs={24} sm={24} md={24} lg={24} xl={8}>
                            <Task
                              name="Công việc đã hoàn thành"
                              amount={`${completed} tasks`}
                              process="Công việc đã được xử lý"
                              color="rgb(0, 171, 111)"
                            />
                        </Col>
                    </Row>
                    <Row className="row-2">
                        <Col xs={24} sm={24} md={24} lg={24} xl={16}>
                            <CardTypo
                              name={currentDate}
                              text={quote}
                            />
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={24} xl={8}>
                            <Card className="history_exit" type="inner" title="Lịch sử ra vào" extra={<a href="">Xem tất cả</a>}>

                            </Card>
                        </Col>
                    </Row>
                </Col>
                <Col span={6}>
                    <RankComponent />
                    <Row>
                        <TemperatureComponent />
                    </Row>
                </Col>
            </Row>
        </HomePage>
    )
}
Home.getLayout = function getLayout(page) {
    return (
        <AppLayOut>{page}</AppLayOut>
    )
}

export default Home;


