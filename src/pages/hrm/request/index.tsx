import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import RequestLayout from "@/layouts/RequestLayout";
import Head from "next/head";

const RequestWrapper = styled.div`
  
`
const Request : NextPageWithLayout = () => {
    return (
        <RequestWrapper>
          <Head>
            <title>Request</title>
          </Head>
        </RequestWrapper>
    )
}
Request.getLayout = function (page) {
    return (
        <AppLayOut>
            <RequestLayout>
                {page}
            </RequestLayout>
        </AppLayOut>
    )
}
export default Request;