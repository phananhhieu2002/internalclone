import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import MenuLayout from "@/layouts/Menu_Layout";
import {propsType} from "@/@types/global";
import {EditOutlined , SendOutlined} from "@ant-design/icons";
import React from "react";
import {Button, Card, Form} from "antd";
import TextArea from "@/components/share-components/TextArea";
import InputComponent from "@/components/share-components/Input";


const ReportWrapper = styled.div`
  .ant-card{
    border: none;
  }
  .ant-card-body h4{
    font-size: 17px;
    margin-bottom: 1.5rem !important;
      font-weight: 700;
  }
  .ant-btn{
    float: right;
  }
`
function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}
const items : propsType = [
    getItem('Báo cáo đã gửi', '/hrm/report/sent', <SendOutlined />),
]
const Report: NextPageWithLayout = () => {
    return (
        <ReportWrapper>
            <Card>
                <h4>Tạo bản báo cáo</h4>
                <Form
                    layout="vertical"
                >
                    <TextArea label="Nội dung báo cáo" />
                    <InputComponent label="Ý kiến đóng góp" placeholderText="Nhập ý kiến đóng góp" />
                    <Button type="primary">Gửi</Button>
                </Form>
            </Card>
        </ReportWrapper>
    )
}

Report.getLayout = function (page) {
    return (
        <AppLayOut>
            <MenuLayout items={items} icon={<EditOutlined />} text="Viết báo cáo">
                {page}
            </MenuLayout>
        </AppLayOut>
    )
}
export default Report;