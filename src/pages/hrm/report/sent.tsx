import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import MenuLayout from "@/layouts/Menu_Layout";
import React from "react";
import {propsType} from "@/@types/global";
import {EditOutlined, SendOutlined} from "@ant-design/icons";
import styled from "styled-components";
import {Row, Select} from "antd";

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}
const items : propsType = [
    getItem('Báo cáo đã gửi', '/hrm/report/sent', <SendOutlined />),
]
const SentContainer = styled.div`
    .header{
      justify-content: space-between;
    }
`
const Sent : NextPageWithLayout = () => {
    return (
        <SentContainer>
            <Row className="header">
                <h1 className="heading">Báo cáo đã gửi</h1>
                <Select
                    style={{ width: 120 }}
                    defaultValue="week"
                    options={[
                        {value : 'week',label : 'Tuần qua'},
                        {value : 'month',label : 'Tháng qua'},
                        {value : 'alltime',label : 'Tất cả'}
                    ]}
                />
            </Row>
            <Row>

            </Row>
        </SentContainer>
    )
}

Sent.getLayout = function getLayout(page){
    return (
        <AppLayOut>
            <MenuLayout items={items} icon={<EditOutlined />} text="Viết báo cáo" keyProp="/hrm/report/new">
                {page}
            </MenuLayout>
        </AppLayOut>
    )
}
export default Sent;