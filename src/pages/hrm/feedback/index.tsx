import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import Head from "next/head";
import {useSelector} from "react-redux";
import {RootState} from "@/redux/store";

const FeedbackBane = styled.div`
`
const FeedBack : NextPageWithLayout = () => {
  const user = useSelector((state : RootState) => state.user);
    return (
        <FeedbackBane>
            <Head>
                <title>Feedback</title>
            </Head>
            <h1>
              {
                `Nhân viên ${user.fullName}`
              }
            </h1>
        </FeedbackBane>
    )
}
FeedBack.getLayout = function getLayout(page){
    return(
        <AppLayOut>
            {page}
        </AppLayOut>
    )
}
export default FeedBack;