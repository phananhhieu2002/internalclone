import styled from "styled-components";
import {NextPageWithLayout} from "@/pages/_app";
import AppLayOut from "@/layouts/App_Layout";
import MenuLayout from "@/layouts/Menu_Layout";
import React from "react";
import {propsType} from "@/@types/global";
import RegulationLayout from "@/layouts/regulation_layout";


interface DataType {
    key: string;
    name: string;
    age: number;
    address: string;
    tags: string[];
}



const RegulationContainer = styled.div`
  
`
function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
    type?: 'group',
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
        type,
    } as MenuItem;
}
const items : propsType = [
    getItem('Thưởng', '/hrm/payment-regulation/bonus'),
    getItem('Phạt', '/hrm/payment-regulation/foul'),
    getItem('Trợ cấp', '/hrm/payment-regulation/support'),
    getItem('Quy định khác', '/hrm/payment-regulation/rule'),
]
const PaymentRegulation : NextPageWithLayout = () => {
    return (
        <AppLayOut>
            <MenuLayout items={items} text="Thêm quy định">
                <RegulationLayout title="Trợ cấp" />
            </MenuLayout>
        </AppLayOut>
    )
}

export default PaymentRegulation;