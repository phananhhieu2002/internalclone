import {createSlice , PayloadAction} from "@reduxjs/toolkit";
import {Account} from "@/@types/auth";

const initialState : Account = {
	_id : '',
	id : '',
	avatar : '',
	fullName : '',
	role : '',
	exp : 0,
	stars : 0,
	email : ''
}

export const userSlice = createSlice({
	name : 'user',
	initialState,
	reducers : {
		setUser : (state , action : PayloadAction<Account>) => {
			state._id = action.payload._id;
			state.id = action.payload.id;
			state.avatar = action.payload.avatar;
			state.fullName = action.payload.fullName;
			state.role = action.payload.role;
			state.exp = action.payload.exp;
			state.stars = action.payload.stars;
			state.email = action.payload.email;
		}
	}
})

export const {setUser} = userSlice.actions;
export default userSlice.reducer;