import {createSlice , PayloadAction} from "@reduxjs/toolkit";
import {taskType} from "@/@types/global";
import { deleteTask, getTasks } from "../actions/task";

const initialState : taskType = {
    data : [],
    pagination : {
        total : 0,
        page : 1,
    },
    information : {
        name : "",
        description : "",
        members : [],
        status : null,
        deadline : null
    }
}

const task2Slice = createSlice({
    name : "task",
    initialState,
    reducers : {
    },
    extraReducers: builder => {
      
        builder.addCase(getTasks.fulfilled, (state, action) => {
          state.data = action.payload.data;
        })
       
        // builder.addCase(createShortLink.fulfilled, (state, action) => {
        //   state.entities = [action.payload.data, ...state.entities]
        //   state.loading = false;
        // })
    
        // builder.addCase(updateShortLink.fulfilled, (state, action) => {
        //   const updateValues = action.payload.data
        //   const findIndex = state.entities.findIndex(item => item._id === updateValues._id)
        //   if(findIndex === -1) return
        //   state.entities[findIndex] = {...state.entities[findIndex], ...updateValues}
        //   state.loading = false;
        // })
    
        builder.addCase(deleteTask.fulfilled, (state, action) => {
        //   state.entities = state.entities.filter(item => item._id !== action.payload.id)
        //   state.loading = false;
          state.data = state.data.filter((item) => item.id !== action.payload)
        })
    
      }
})


export const {} = task2Slice.actions;
export default task2Slice.reducer;