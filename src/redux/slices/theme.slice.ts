import { createSlice } from '@reduxjs/toolkit';

export interface ColorType {
    value : boolean
}

const initialState: ColorType = {
    value: false,
}

export const ThemeSlice = createSlice({
    name : "theme",
    initialState,
    reducers : {
        takeTheme : (state  , action ) => {
            state.value = action.payload;
        }
    }
})

export const {takeTheme} = ThemeSlice.actions;

export default ThemeSlice.reducer;