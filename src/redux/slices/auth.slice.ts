import {createSlice} from '@reduxjs/toolkit';

export interface AuthState {
    login: {
        currentUser: any,
        isFetching: boolean,
        error: boolean
    }
}

const initialState: AuthState = {
    login: {
        currentUser: null,
        isFetching: false,
        error: false
    }
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        loginStart: (state) => {
            state.login.isFetching = true;
        },
        loginSuccess: (state, action) => {
            state.login.isFetching = false;
            state.login.currentUser = action.payload;
            state.login.error = false;
        },
        loginFailed: (state) => {
            state.login.isFetching = false;
            state.login.error = true;
        }
    },
    extraReducers: builder => {
    }
});

export const {
    loginStart,
    loginSuccess,
    loginFailed
} = authSlice.actions;

export default authSlice.reducer;
