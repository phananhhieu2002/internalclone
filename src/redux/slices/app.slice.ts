import {createSlice} from '@reduxjs/toolkit';

export interface AppState {
  bgrStatus : boolean,
  color : string,
  modal : boolean,
  page : number,
  isModalOpen : boolean
}

const initialState: AppState = {
  bgrStatus : true,
  color : 'Light',
  modal : false,
  page : 1,
  isModalOpen : false
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    push: (state) => {
      state.bgrStatus = true;
    },
    pull: (state) => {
      state.bgrStatus = false;
    },
    light : (state) => {
      state.color = 'Light';
    },
    dark : (state) => {
      state.color = 'Dark';
    },
    open : (state) => {
      state.modal = true;
    },
    close : (state) => {
      state.modal = false;
    },
    setPage : (state, action) => {
      state.page = action.payload
    },
    setIsModalOpen : (state,action) => {
      state.isModalOpen = action.payload;
    }
  },
  extraReducers: builder => {

  }
});

export const {
  push,
  pull ,
  light ,
  dark,
  open,
    close,
  setPage,
  setIsModalOpen
} = appSlice.actions;

export default appSlice.reducer;
