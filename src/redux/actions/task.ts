import ApiService from "@/service/ApiService";
import { TaskFilter } from "@/@types/global";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getTasks = createAsyncThunk('tasks/get', async (query: {
    page: number, sortBy?: string
}) => {
    const { page, sortBy } = query
    const tasksFilter: TaskFilter = {
    }
    const data = await ApiService.getGeneralTasks({
        ...tasksFilter,
        page,
        sortBy,
        limit: 10,
    })
    return data
})

export const deleteTask = createAsyncThunk('tasks/delete', async (id : string) => {
    await ApiService.deleteTask(id);
    return id;
})