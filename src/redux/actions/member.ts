import ApiService from "@/service/ApiService";
import {getAllMembers} from "@/redux/slices/member.slice";


export const getMembers = () => async dispatch => {
	try{
		const res = await ApiService.searchUser({ limit: 99 });
		dispatch(getAllMembers(res.data));
	}
	catch (err){
		console.log('Get members is error !!! ');
	}
}