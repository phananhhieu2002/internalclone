import ApiService from "@/service/ApiService";

import {setUser} from "@/redux/slices/user.slice";


export const getMyUser = () => async dispatch => {
	try {
		const response = await ApiService.getSelfAccount();
		dispatch(setUser(response.data));
	}
	catch(err){
		console.log('Get my user failure !!!');
	}
}