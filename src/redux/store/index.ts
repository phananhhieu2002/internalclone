import {configureStore} from "@reduxjs/toolkit";
import themeReducer from "@/redux/slices/theme.slice";
import appReducer from "@/redux/slices/app.slice";
import authReducer from "@/redux/slices/auth.slice";
import userReducer from "@/redux/slices/user.slice";
import memberReducer from "@/redux/slices/member.slice";
import taskReducer from "@/redux/slices/task2.slice";
export const store = configureStore({
  reducer : {
    auth : authReducer,
    app : appReducer,
    theme : themeReducer,
    user : userReducer,
    member : memberReducer,
    task : taskReducer
  }
})

export type RootState = ReturnType<typeof store.getState>;
