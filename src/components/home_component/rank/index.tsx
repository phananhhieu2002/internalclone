import styled from "styled-components";
import {Progress} from "antd";
import {memo, useEffect, useState} from "react";
import * as User from "@/redux/actions/user";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";


const Rank = styled.div`
  background-color: #fafafb;
  border: 1px solid #ccc;
  border-radius: 7px;

  .ant-card-body {
    padding: 20px;
  }

  .ant-progress-bg {
    width: 5%;
    height: 6px;
  }

  .link_actor {
    float: right;
    font-size: 11px;
  }

  .img_actor {
    width: 48px;
    height: 48px;
  }

  .icon {
    background-color: rgb(56, 56, 56);
    color: rgb(255, 255, 255);
    border-radius: 10px;
    padding: 2px 5px;
    float: right;
    font-size: 11px;
    line-height: 11px;
  }

  .content-center {
    display: flex;
  }

  .actor {
    width: 100%;
    margin-left: 10px;
  }

  .user {
    font-weight: bold;
    font-size: 14px;
    color: #1b2531 !important;
  }

  .actor_line {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: 15px;
    color: #72849a;
    font-weight: 500;
  }

  .ant-progress-bg {
    width: 100%;
    height: 5px !important;
  }

  .ant-progress-inner {
    margin-bottom: 10px;
  }
`

const RankComponent = () => {
	const user = useSelector((state : RootState) => state.user);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(User.getMyUser());
	},[user])
	return (
		<Rank className="ant-card ant-card-bordered">
			<div className="ant-card-body">
				<div className="d-flex flex-row content-center">
					<div className="tier-icon d-flex align-items-center">
						<img
							className="img_actor"
							src="https://i.imgur.com/OrTJEiv.png"
							alt="rank"
						/>
					</div>
					<div className="actor  flex-column ml-3 flex-grow-1">
						<div className="actor_line">
                            <span className="d-flex font-weight-bold align-items-center justify-content-between">
                                <span className="user">{user.fullName}</span><br/>
                                Rookie
                            </span>
							<span className="tier d-flex align-items-end justify-content-between">
                                <div className="icon">⭐ {user.stars}</div> <br/>
                                <a className="link_actor" href="/leveling">{user.stars}/20</a>
                            </span>
						</div>
						<div className="tier-progress">
							<div
								className="ant-progress ant-progress-line ant-progress-status-normal ant-progress-small">
								<div className="ant-progress-outer">
									<div className="ant-progress-inner">
										<div className="ant-progress-bg">
											<Progress percent={80} showInfo={false}/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Rank>
	)
}
export default memo(RankComponent);