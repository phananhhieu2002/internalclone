import styled from "styled-components";
import axios from "axios";
import {useLayoutEffect, useState} from "react";
import {API_KEY , BASE_URL} from "@/configs/weather.config";
import Utils from "@/utils";
const FututreTemperatureContainer = styled.div`
	color: var(--light);
	.future_wrapper{
		width: 100%;
		height: 175px;
		position: relative;
	}
	.top{
		height: 16px;
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		z-index: 1;
    background: linear-gradient(rgb(77, 52, 242) 0%, rgba(0, 212, 255, 0) 100%);
	}
	.bottom{
    height: 16px;
    position: absolute;
    left: 0;
    right: 0;
		bottom: 0;
    z-index: 1;
    background: linear-gradient(rgb(77, 52, 242) 0%, rgba(0, 212, 255, 0) 100%);
	}
	.info_forecast{
		padding-top: 16px;
		height: 160px;
		overflow-y: scroll;
	}
  .info_forecast::-webkit-scrollbar{
    display: none;
  }
	.forecast_item{
		display: flex;
		margin-bottom: 16px;
		justify-content: space-between;
	}
	.info_degree{
		display: flex;
		align-items: center;
	}
	.day_in_week{
		font-size: 19px;
		font-weight: 500;
	}
	.current_date{
    opacity: 0.5;
    color: rgb(255, 255, 255);
		font-weight: 500;
		font-size: 14px;
	}
	.degree_start{
		opacity: 0.75;
	}
	.degree_end,
  .degree_start{
		margin-left: 16px;
		font-size: 18px;
		font-weight: 500;
	}
	.icon_indicate img{
		width: 32px;
		height: 32px;
	}
`

const FutureTemperature = () => {
	const [address,setAddress] = useState("Hanoi")
	const [data,setData] = useState(null);
	const getForecastWeather = async () => {
		try {
			const response = (await axios.get(`${BASE_URL}/forecast.json?key=${API_KEY}&q=${address}&days=3`)).data.forecast.forecastday
			setData(response);
		}
		catch (error){
			console.log(error);
		}
	}
	const returnDayInWeek = (idx : number) => {
		if(idx == 0){
			return 'Hôm nay';
		}
		else if(idx == 1){
			return 'Ngày mai';
		}
		else{
			return 'Ngày kia';
		}
	}
	useLayoutEffect(() => {
		getForecastWeather();
	},[])
	return (
		<FututreTemperatureContainer>
			<div className="future_wrapper">
				<div className="top"></div>
				<div className="bottom"></div>
				<div className="info_forecast">
					{/*Demo*/}
					{data && data.map((item,index) => (
						<div className="forecast_item">
							<div className="date">
								<div className="current_date">{Utils.convertDate(item.date.slice(5,item.date.length + 1))}</div>
								<div className="day_in_week">
									{returnDayInWeek(index)}
								</div>
							</div>
							<div className="info_degree">
								<div className="icon_indicate">
									<img src={item.day.condition.icon} alt=""/>
								</div>
								<div className="degree_end">
									{item.day.maxtemp_c + "°C"}
								</div>
								<div className="degree_start">
									{item.day.mintemp_c + "°C"}
								</div>
							</div>
						</div>
					))}
				</div>
			</div>
		</FututreTemperatureContainer>
	)
}
export default FutureTemperature;