import styled from "styled-components";
import {HomeOutlined} from "@ant-design/icons";
import {BASE_URL , API_KEY} from "@/configs/weather.config";
import {useEffect, useState} from "react";
import axios from "axios";
import text from "parchment/src/blot/text";

const TodayTemperature = styled.div`
  .today_temperature{
    display: flex;
    color: var(--light);
  }
  .country{
    font-size: 18px;
    margin-left: 8px;
    font-weight: 500;
  }
  .address{
    display: flex;
    align-items: center;
  }
  .temperature{
    font-size: 48px;
    font-weight: 900;
  }
  .illustration{
    width: 100%;
  }
  .status {
    text-align: right;
  }
  .parameter{
    min-width: 200px;
  }
  .status{
    font-weight: 500;
    font-size: 16px;
  }
  .illustration{
    display: flex;
    flex-direction: column;
  }
  .iconImg{
    float: right;
  }
`


const TodayTemperatureComponent = () => {
  const [address,setAdress] = useState("Hanoi");
  const [degree,setDegree] = useState(0);
  const [icon,setIcon] = useState("");
  const [status,setStatus] = useState("");
  const getCurrentWeather = async () => {
    try {
      const response = await axios.get(`${BASE_URL}/current.json?key=${API_KEY}&q=${address}`);
      setDegree(response.data.current.temp_c)
      setIcon(response.data.current.condition.icon)
      setStatus(response.data.current.condition.text);
    }
    catch(error){
      console.log(error)
    }
  }
  useEffect(() => {
    getCurrentWeather();
  },[]);
    return (
        <TodayTemperature>
           <div className="today_temperature">
             <div className="parameter">
               <div className="address">
                 <HomeOutlined />
                 <span className="country">{address}</span>
               </div>
               <div className="temperature">
                 {degree >= 0 ? `+${degree}°C` : `-${degree}°C`}
               </div>
             </div>
             <div className="illustration">
                <div className="icon">
                  <img className="iconImg" src={icon} alt="" />
                </div>
               <div className="status">{status}</div>
             </div>
           </div>
        </TodayTemperature>
    )
}
export default TodayTemperatureComponent;
