import styled from "styled-components";
import TodayTemperatureComponent from "@/components/home_component/Temperature/TodayTemperature";
import {Card} from "antd";
import FutureTemperature from "@/components/home_component/Temperature/FutureTemperature";

const Temperature = styled.div`
  padding: 20px;
  background: rgb(77, 52, 242);
  margin-top: 10px;
  color: #fff;
  border-radius: 7px;
  width: 100%;
  .ant-card{
    background-color: transparent;
    border: none;
  }
  .ant-card-body{
    padding: 10px;
  }
`

const TemperatureComponent = () => {
    return (
        <Temperature>
            <Card>
                <TodayTemperatureComponent />
                <FutureTemperature />
            </Card>
        </Temperature>
    )
}
export default TemperatureComponent;
