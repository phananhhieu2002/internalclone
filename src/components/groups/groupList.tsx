import styled from "styled-components";
import {Group} from "@/@types/global";
import {Avatar, Card, Col, Dropdown, Menu, message, Row, Tag} from "antd";
import {DeleteOutlined, EllipsisOutlined} from "@ant-design/icons";
import ApiService from "@/service/ApiService";

const Styled = styled.div`

`
const GroupList = ({groups} : {groups : Group[]}) => {
	const [messageApi, contextHolder] = message.useMessage();
	const success = () => {
		message.success("Rời khỏi nhóm thành công")
	};
	const handleDelete = async (id : string) => {
		try{
			await ApiService.deleteGroup(id);
			success();
		}
		catch (err){
			console.log(err);
		}
	}
	return (
		<>
			{
				groups && groups.map(group => (
					<Col xs={24} md={12}>
						<Card className="group_item">
							<Row className="line_1">
								<div className="group_header">
									<div className="group_header_logo">
										<Avatar
											src={group.avatar}
										/>
									</div>
									<div className="group_header_info">
										<div className="other">
											<h5 className="group_header_title">{group.name}</h5>
											<span>{`${group.memberCount} thành viên`}</span>
										</div>
									</div>
								</div>
								<Dropdown
									trigger={['click']}
									placement={'bottomRight'}
									overlay={
										<Menu>
											<Menu.Item>
												<div
													style={{display : "flex" , alignItems : "center"}}
													onClick={() => {
														handleDelete(group.id)
													}}
												>
													<DeleteOutlined className="mr-2" />
													<span style={{marginLeft : 7}}>Rời khỏi nhóm</span>
												</div>
											</Menu.Item>
										</Menu>
									}
								>
									<EllipsisOutlined style={{ fontSize: 30, color: '#8c8c8c' }} />
								</Dropdown>
							</Row>
							<Row className="description">
								{group.description}
							</Row>
							<Row>
								<div className="inform_group">
									<Tag bordered={false} color="success">{group.postCount + " bài viết"}</Tag>
									<Tag bordered={false} color="processing">{group.memberCount + " thành viên"}</Tag>
								</div>
							</Row>
						</Card>
					</Col>
				))
			}
		</>
	)
}

export default GroupList;