import styled from "styled-components";
import {useEffect, useRef, useState} from "react";
import {Group} from "@/@types/global";
import { Input, Row, Spin} from "antd";
import { SearchOutlined} from "@ant-design/icons";
import Utils from "@/utils";
import ApiService from "@/service/ApiService";
import PaginationComponent from "@/components/share-components/Pagination";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import GroupList from "@/components/groups/groupList";
import {log} from "next/dist/server/typescript/utils";

const Styled = styled.div`
  .gropup_In{
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 24px;
  }
  .gropup_In .part{
    font-size: 20px;
    font-weight: 600;
  }
	.ant-card{
		//border: 1px solid #e6ebf1 !important;
	}
	.ant-row{
		justify-content: space-between;
		align-items: center;
	}
	.ant-col{
		padding: 8px;
	}
	.mr-2{
		margin-right: 5px;
	}
	.line_1{
		border-bottom: 1px solid #e6ebf1;
		padding-bottom: 8px;
	}
	.group_joined{
		margin-top: 25px;
	}
	.ant-avatar{
		width: 40px;
		height: 40px;
	}
	.ant-card-body{
		padding: 15px 15px 24px !important; 
	}
	.description{
		min-height: 130px;
		padding: 10px;
		align-items: flex-start !important;
    font-size: 16px;
		font-weight: 500;
		color: #455560;
	}
	.ant-tag{
		border-radius: 15px;
		height: 25px;
	}
	.group_joined .other h5{
		font-size: 18px;
	}
  .group_joined .other span{
   font-weight: 400;
  }
	.line_1:hover{
		cursor: pointer;
	}
	.anticon{
		margin-right: 5px;
	}
  .ant-card{
    box-shadow: 0 0 2px #ccc !important;
  }
	.mr-2{
		margin-right: 10px;
	}
	.input{
		font-weight: 500;
	}
	.ant-avatar img{
		width: 100% !important;
		height: 100% !important;
		border-radius: 100%;
		object-fit: cover;
	}
	.group_header{
		display: flex;
		//max-width: 188px;
	}
	.group_header_logo{
		display: flex;
		margin-right: 10px;
		align-items: center;
	}
	.group_header_info{
		display: flex;
		align-items: center;
	}
	.group_header_title{
		line-height: 1;
		font-weight: 650 !important;
		font-size: 15px !important;
	}
	.ant-tag{
		font-size: 12px;
	}
	.loading{
		width: 100%;
		display: flex;
		justify-content: center;
	}
`
const JoinedGroup = () => {
	const searchRef = useRef<any>(null);
	const pageCurrent = useSelector((state : RootState) => state.app.page);
	const dispatch = useDispatch();
	const [groups,setGroups] = useState<Group[]>([]);
	const [keyword , setKeyword] = useState('');
	const [total , setTotal] = useState(0);
	const [currentPage , setCurrentPage] = useState(1);
	const [loading , setLoading] = useState(false);
	const getGroups = async (page = pageCurrent, slug = keyword, limit = 4, sortBy = '-createdAt') => {
		try {
			setLoading(true);
			const queries = { page, limit, sortBy };
			slug.trim().length > 0 && Object.assign(queries, { slug: Utils.slugify(slug) });
			const res = await ApiService.getGroups(queries);
			setGroups(res.data);
			setCurrentPage(res.pagination.page);
			setTotal(res.pagination.total);
		} catch (err) {
			console.log(err);
		}
		setLoading(false);
	};
	// const calculatePage = () => {
	// 	if(groups.length <= 4){
	// 		setTotal(4)
	// 	}
	// }
	// const handleFilter = async (e : any) => {
	// 	if(e.target.value === ""){
	// 		setLoading(true);
	// 		const res = await ApiService.getGroups({page : 1 , limit : 4,sortBy : '-createdAt'});
	// 		setGroups(res.data);
	// 	}
	// 	else{
	// 		setLoading(true);
	// 		const filterGroup = groups.filter(group => (
	// 			group.name.includes(e.target.value)
	// 		))
	// 		setGroups(filterGroup)
	// 	}
	// 	setLoading(false);
	// 	calculatePage();
	// }
	useEffect(() => {
		getGroups();
	},[pageCurrent]);
	return (
		<Styled>
			<div className="gropup_In">
				<span className="part">Nhóm đã tham gia</span>
				<Input
					size="small"
					className="input"
					placeholder="Tìm kiếm..."
					prefix={<SearchOutlined />}
					// onInput={handleFilter}
					onInput={e => {
						const value = e.target.value;
						setKeyword(value);
						if (searchRef.current) {
							clearTimeout(searchRef.current);
						}
						searchRef.current = setTimeout(() => {
							getGroups(1, value);
						}, 500);
					}}
				/>
			</div>
			<div className="group_joined">
				<Row>
					{
						!loading ? (<GroupList groups={groups} />) :
						(
							<div className="loading">
								<Spin size="large" />
							</div>
						)
					}
				</Row>
				<PaginationComponent current={pageCurrent} item={4} total={total} />
			</div>
		</Styled>
	)
}

export default JoinedGroup;