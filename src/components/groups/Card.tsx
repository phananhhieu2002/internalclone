import styled from "styled-components";
import {Card} from "antd";
import ItemCard from "@/components/share-components/ItemCard";
import {
    UsergroupAddOutlined ,
    SaveOutlined ,
    ApartmentOutlined,
    SettingOutlined
} from '@ant-design/icons';
const CardContainer = styled.div`
  margin-bottom: 20px;
  border-radius: 7px;
    .cardHeader{
      font-size: 21px;
      margin: 10px;
        font-weight: 500;
    }
  
  .anticon{
    //height: 24px;
  }
  .avatar{
    padding: 0 10px;
  }
  .ant-avatar-circle{
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .ant-card-body{
    padding: 10px !important;
  }
`
const CardComponent = () => {
    return (
        <CardContainer>
            <Card>
                <h1 className="cardHeader">Danh Mục</h1>
                <div className="list-item-card">
                    <ItemCard title="Nhóm đã tham gia " icon={<UsergroupAddOutlined />} des="30 người"/>
                    <ItemCard title="Bài viết đã lưu " icon={<SaveOutlined />} des="30 bài" />
                    <ItemCard title="Chức năng khác " icon={<ApartmentOutlined />} des="30 người"/>
                    <ItemCard title="Tuỳ chọn" icon={<SettingOutlined />} des="30 người"/>
                </div>
            </Card>
        </CardContainer>
    )
}

export default CardComponent;