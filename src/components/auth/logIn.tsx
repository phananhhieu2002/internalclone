import {Button, Form, Input} from "antd";
import {LockOutlined, UserOutlined} from "@ant-design/icons";
import styled from "styled-components";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {authService} from "@/service/auth.service";
const FormWrappe = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  .login-form{
    min-width: 250px;
    max-width: 400px;
    box-shadow: 0 0 6px #ccc;
    padding: 32px 40px;
    border-radius: 10px;
  }
  .form_heading{
    font-size: 30px;
    font-weight: 700;
    text-align: center;
    margin-bottom: 20px;
  }
  .ant-btn{
    width: 100%;
    margin: 20px 0;
  }
`
const LogInForm = () => {
    const [email , setEmail] = useState("");
    const [password , setPassword] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const handleSubmit = (e : any) => {
        let loginDialog: any;
        const isCordova = !!window.cordova;
        if (isCordova) {
            loginDialog = window.cordova['InAppBrowser'].open(authService.generateAuthUrl());
        } else {
            loginDialog = window.open( authService.generateAuthUrl(), '_blank');
        }
        const authorizationInterval = setInterval(async () => {
            try {
                let search = '';
                if (isCordova) {
                    search = await new Promise(_ => {
                        loginDialog.executeScript(
                          {
                              code: `window.location.search`,
                          },
                          (values: string[]) => _(values[0])
                        );
                    });
                    console.log(search);
                } else {
                    search = loginDialog?.location.search;
                }
                if (search.includes('code=')) {
                    clearInterval(authorizationInterval);
                    const searchQuery = search
                      .replace('?', '')
                      .split('&')
                      .find(x => x.includes('code='));
                    if (searchQuery) {
                        const code = searchQuery.split('=')[1];
                        if (code) {
                            loginDialog.close();
                            const data = await authService.requestToken(code, "");
                            console.log(data)
                            localStorage.setItem("token", JSON.stringify(data))
                            // window.location.href = '/home';
                        } else {
                            throw new Error('Auth error: Invalid code.');
                        }
                    } else {
                        throw new Error('Auth error.');
                    }
                }
            } catch (e) {
                console.log(e);
                console.log('--> Sign In on progress');
            }
        }, 500);
    }

    }
    return (
        <FormWrappe>
            <Form
                name="normal_login"
                className="login-form"
                layout={"vertical"}
                initialValues={{ remember: true }}
                onFinish={handleSubmit}
            >
                <h2 className="form_heading">Sign in</h2>
                <Form.Item
                    name="email"
                    label="Email"
                    rules={[{ required: true, message: 'Please input your Email!' }]}
                >
                    <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email"
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    label="Password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Sign in
                    </Button><br/>
                    Do not have an account? <a href="">Create an account!</a>
                </Form.Item>
            </Form>
        </FormWrappe>

    )
}
export default LogInForm;