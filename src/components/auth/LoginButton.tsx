import {Button, Form} from "antd";

const LoginButton = () => {
    return (
        <Form>
            <Form.Item>
                <Button type="primary" htmlType="submit" block>
                    Sign In with NorthStudio Account
                </Button>
            </Form.Item>
        </Form>
    )
}
export default LoginButton;