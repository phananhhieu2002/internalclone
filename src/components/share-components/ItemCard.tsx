import styled from "styled-components";
import {PropType} from "@/@types/global";
import {Avatar} from "antd";
import Image from "next/image";

const CardItem = styled.div`
  display: flex;
  align-items: center;
  padding: 8px 0;

  .other h5 {
    font-weight: 500;
    font-size: 16px;
  }

  .list-item-card div {
    padding: 8px 0 !important;
  }

  .other {
    margin-left: 10px;
  }

  .other span {
    font-weight: 400 !important;
    opacity: 0.8;
    font-size: 14px;
    color: var(--fadeColor);
  }

  .ant-avatar {
    background-color: #f0f2f5;
    width: 35px;
    height: 35px;
    color: #1677ff;
  }

  .avatar {
    height: fit-content;
  }
  .anticon{
    font-size: 18px;
  }
`

const ItemCard = ({title, icon, des, source}: PropType) => {
    const url : string = 'https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg';
    return (
        <CardItem className="card_wrap">
            <span className="avatar">
              {icon ? (<Avatar size="large" icon={icon}/>) :
                  <Avatar src={url} />
              }
            </span>
            <div className="other">
                <h5>{title}</h5>
                <span>{des}</span>
            </div>
        </CardItem>
    )
}
export default ItemCard;