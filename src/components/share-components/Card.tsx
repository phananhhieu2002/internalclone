import styled from "styled-components";
import {Card} from "antd";
import {PropType} from "@/@types/global";
import ItemCard from "@/components/share-components/ItemCard";

const CardContainer = styled.div`
    .cardHeader{
      font-size: 20px;
      margin: 10px;
      font-weight: 500;
    }
  
  .anticon{
    height: 24px;
  }
  .avatar{
    padding: 0 10px;
  }
  .ant-avatar-circle{
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .ant-card-body{
    padding: 10px !important;
  }
`
const CardCommonComponent = (props : PropType) => {
    const {title} = props;
    return (
        <CardContainer>
            <Card>
                <h1 className="cardHeader">{title}</h1>
                <div className="list-item-card">
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                    <ItemCard title="Fan Manchester City" des="2 Bài viết mới" />
                </div>
            </Card>
        </CardContainer>
    )
}

export default CardCommonComponent;