import styled from "styled-components";
import { Menu } from 'antd';
import React from "react";
import {propsType} from "@/@types/global";

const MenuStyled = styled.div`

`

const MenuComponent = (props : propsType) => {
  const {items} = props;
  return(
    <MenuStyled>
      <Menu
        mode="inline"
        defaultSelectedKeys={['/']}
        items={items}
      />
    </MenuStyled>
  )
}
export default  MenuComponent;