import { Card } from "antd"
import { PropType } from "@/@types/global";
import styles from './Task.module.css';
import {useRouter} from "next/router";
import styled from "styled-components";
const Container = styled.div`
  .ant-card-head{
    border: none;
    margin-top: 3px;
    font-weight: 500;
    font-size: 19px;
  }
  .ant-card-body{
    padding: 0 24px 24px;
  }
  .ant-card-head-title{
    font-size: 18px;
    font-weight: 600;
  }
  .ant-card{
    max-height: 153px;
  }
`
const Task = (props: PropType) => {
    const { name, amount, process, color } = props;
    const router = useRouter();
    const linkToTask = () => {
        router.push("/tasks");
    }
    return (
      <Container>
        <Card title={name} bordered={true} style={{ width: 390 }} className={styles.card} onClick={linkToTask}>
          <h1 className={styles.heading} style={{color : `${color}`}}>{amount}</h1>
          <p className={styles.para}>{process}</p>
        </Card>
      </Container>
    )
}
export default Task;