import styled from "styled-components";
import {Form} from "antd";
import ReactQuill from "react-quill";
import 'react-quill/dist/quill.snow.css';

interface prop{
    label : string
}
const TextAreaContainer = styled.div`
  .ql-container{
    min-height: 200px;
  }
`
const TextArea = ({label} : prop) => {
    const modules = {
        toolbar: [[{header: [1, 2, false]}], ['bold', 'italic', 'underline'], ['image', 'code-block']],
    };
    return (
        <TextAreaContainer>
            <Form.Item
                name="description"
                label={label}
                rules={[{required: true, message: `Vui lòng nhập ${label}!`}]}
            >
                <ReactQuill
                    modules={modules}
                    theme="snow"
                />
            </Form.Item>
        </TextAreaContainer>
    )
}
export default TextArea;