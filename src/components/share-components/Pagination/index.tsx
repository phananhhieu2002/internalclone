import styled from "styled-components";
import {Pagination} from "antd";
import {useState} from "react";
import type { PaginationProps } from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import {setPage} from "@/redux/slices/app.slice";


const Styled = styled.div`
  .ant-pagination{
    float: right;
    margin-top: 20px;
    font-weight: 400 !important;
  }
    .ant-pagination-item-active{
        background-color: #1677ff;
    }
    .ant-pagination-item-active a{
        color: #ffffff;
    }
`
const PaginationComponent = ({total , item , current} : {total : number , item : number , current : number} ) => {
    const dispatch = useDispatch();
    const [currentPage , setCurrentPage] = useState<number>(current);
    const onChange : PaginationProps['onChange'] = (page) => {
        setCurrentPage(page);
        dispatch(setPage(page));
    }
    return (
        <Styled>
            <Pagination current={currentPage} defaultPageSize={item} onChange={onChange} total={total} />
        </Styled>
    )
}

export default PaginationComponent;