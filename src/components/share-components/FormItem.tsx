import styled from "styled-components";
import {Checkbox, Form, Input, Select} from "antd";
import React from "react";


const {TextArea} = Input;
const ItemForm = styled.div`
  .ant-form-item-label{
    margin-right: 10px;
    word-break: break-all;
  }
  
`
type options = {
    value : string,
    label : string
}
export type FormItem = {
    name : string,
    label : string ,
    type : string,
    options? : options[]
}
const FormItem = ({name, label, type , options}: FormItem) => {
    let element;
    if (type === "input") {
        element = <Input placeholder={`${label}...`}/>
    } else if (type === "textarea") {
        element = <TextArea rows={4} placeholder={`${label}...`}/>
    } else if (type === "select") {
        element = <Select
            defaultValue=""
            options={options}
        />
    } else {
        element = <Checkbox >Sự kiện này có lặp lại hay không?</Checkbox>
    }
    return (
        <ItemForm>
            <Form.Item
                name={name}
                label={label}
                rules={[
                    {
                        required: true,
                        message: `Vui lòng nhập ${name} !`,
                    }
                ]}
            >
                {element}
            </Form.Item>
        </ItemForm>
    )
}
export default FormItem;