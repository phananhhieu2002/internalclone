import {Card} from "antd"
import styles from './CardTypo.module.css';
import {PropType} from "@/@types/global";
import styled from "styled-components";

const CardTimer = styled.div`
    .ant-card{
      padding: 15px;
      position: relative;
    }
  .ant-card-body{
    padding: 0 0 0 10px;
    position: absolute;
    bottom: 15px;
  }
`
const CardTypo = (props: PropType) => {
    const {name, text} = props;
    return (
        <CardTimer>
            <Card bordered={true} style={{minHeight: 200, backgroundImage: `url(https://i.imgur.com/eH2DMfe.jpg)`}}
                  className={styles.card}>
                <h1 className={styles.title}>{name}</h1>
                <p className={styles.text}>{text}</p>
            </Card>
        </CardTimer>
    )
}
export default CardTypo;