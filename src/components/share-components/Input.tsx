import styled from "styled-components";
import {Form , Input } from "antd";

interface prop{
    label : string,
    placeholderText : string
}

const InputContainer = styled.div`

`
const InputComponent = ({label , placeholderText} : prop) => {
    return (
        <InputContainer>
            <Form.Item
                name="description"
                label={label}
                rules={[{required : true ,message: `Vui lòng nhập ${label}!`}]}
            >
                <Input style={{padding : "8px 11px" , border : "1px solid #ccc"}} placeholder={placeholderText} />
            </Form.Item>
        </InputContainer>
    )
}
export default InputComponent;