import styled from "styled-components";
import {Radio} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "@/redux/store";
import {dark, light} from "@/redux/slices/app.slice";
import {useState} from "react";
const CategoryWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1.5rem 0;
`
const CategoryColor = () => {
    const ColorCategory = useSelector((state : RootState) => state.app.color);
    const dispatch = useDispatch();
    const options = [
        { label: 'Light', value: 'Light' },
        { label: 'Dark', value: 'Dark' },
    ];
    const handleChange = () => {
        ColorCategory === 'Light' ? dispatch(dark()) : dispatch(light());
    }
    return (
        <CategoryWrapper>
            <div className="settingsLabel">Side Nav Color:</div>
            <div className="switchWrapper">
                <Radio.Group options={options} defaultValue={ColorCategory} onChange={handleChange} optionType="button" />
            </div>
        </CategoryWrapper>
    )
}
export default CategoryColor;