import styled from "styled-components";
import {Switch} from "antd";
import {useDispatch} from "react-redux";
import {takeTheme} from "@/redux/slices/theme.slice";

const ThemeWrapper = styled.div`
    display: flex;
  justify-content: space-between;
  .ant-switch-inner{
    border: 1px solid #cccccc;
  }
`
const ThemeColor = () => {
    const dispatch = useDispatch();
    const onChange = (checked: boolean) => {
        dispatch(takeTheme(checked));
    };
    return (
        <ThemeWrapper>
            <div className="settingsLabel">Dark Theme:</div>
            <div className="switchWrapper">
                <Switch onChange={onChange} />
            </div>
        </ThemeWrapper>
    )
}

export default ThemeColor;