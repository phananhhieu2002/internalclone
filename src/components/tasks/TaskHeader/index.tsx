import {Col, Row, Select} from "antd";
import {CalendarOutlined} from "@ant-design/icons";
import React, {useEffect, useMemo} from "react";
import styled from 'styled-components';
import {useDispatch, useSelector} from "react-redux";
import * as Members from "@/redux/actions/member";
import {RootState} from "@/redux/store";
import {Account} from "@/@types/auth";

const HeaderTask = styled.div`
  .page {
    font-size: 21px;
    font-weight: 500;
  }

  .filter {
    //width: 450px;
  }
  .show-range-time{
    font-weight: 600 !important;
    align-items: center;
  }
  .show-range-time:hover{
    cursor: pointer;
  }
  .filterItem{
    //padding: 0 4px;
  }
`

const TaskHeader = () => {
  const dispatch = useDispatch();
  const members = useSelector((state : RootState) => state.member);
  const allMember = [...members , {fullName : "Toàn bộ thành viên"}];
  const handleChange = (value : string) => {
    console.log(value);
    
  }
  useEffect(() => {
    dispatch(Members.getMembers());
  },[])
    return (
        <HeaderTask>
        <Row
            className="header"
            justify="space-between"
        >
            <h1 className="page">Nhiệm vụ</h1>
            <Col className="filter">
                <Row justify="space-around" style={{minWidth : 500}}>
                    <Col  className="show-range-time filterItem" >
                        <CalendarOutlined className="mr-2" /> Lọc theo thời gian
                    </Col>
                    <Col className="filterItem" >
                        <Select
                            defaultValue="Toàn bộ thành viên"
                            style={{ minWidth : 200 }}
                            options = { allMember && allMember.map(member => ({
                              value : member.fullName ,
                              label : member.fullName
                            }))}
                            onChange={handleChange}
                        >
                        </Select>
                    </Col>
                    <Col className="filterItem"   >
                        <Select
                            defaultValue="Danh sách"
                            style={{ width: 120 }}
                            options={[
                                { value: 'Bảng', label: 'Bảng' },
                                { value: 'Danh sách', label: 'Danh sách' },
                            ]}
                        />
                    </Col>
                </Row>
            </Col>
        </Row>
        </HeaderTask>
    )
}
export default TaskHeader;