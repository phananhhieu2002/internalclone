import styled from 'styled-components';
import {
    Form
    , Input
    , Select
    , Switch
    , Upload
    , Button
    , Card
    , Collapse
    , Row
    , Col, DatePicker
} from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import ReactQuill from "react-quill";
import 'react-quill/dist/quill.snow.css';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@/redux/store';
import { Account } from "@/@types/auth";
import ApiService from "@/service/ApiService";
import Utils from "@/utils";
import { setIsModalOpen } from '@/redux/slices/app.slice';
import { useRouter } from 'next/router';
const { Option } = Select
const { RangePicker } = DatePicker;
const projectList = [];

const TaskForm = styled.div`
  font-weight: 600 !important;

  .heading {
    font-size: 24px;
  }

  .form_wrap {
    margin-top: 10px;
  }

  .ql-container .ql-editor {
    min-height: 180px;
  }

  .btn_Submit {
    width: 100%;
    background-color: #1677ff;
    box-sizing: border-box;
  }

  .mb-2 {
    font-weight: bold;
    margin-bottom: 10px;
  }
    .mr-2{
        margin-right: 6px;
    }
  .card_container {
    margin-bottom: 10px;
  }

  .label {
    margin: 10px 0;
  }

  .confirm {
    //margin-left: 4px;
  }

  .ant-row {
    justify-content: space-between;
  }

  .ant-col {
    padding: 0 8px;
  }

  .ant-form-item-label label {
    margin-bottom: 10px;
  }
`

const FormWrap = ({ title , id }: { title: string , id? : any }) => {
    const [name, setName] = useState<string>("");
    const [description, setDescription] = useState<string>('');
    const [members, setMembers] = useState<Account[]>([]);
    const [status, setStatus] = useState<any>(null);
    const [deadline, setDeadLine] = useState<any>(null);
    const member = useSelector((state: RootState) => state.member);
    const dispatch = useDispatch();
    const router = useRouter()
    const modules = {
        toolbar: [[{ header: [1, 2, false] }], ['bold', 'italic', 'underline'], ['image', 'code-block']],
    };
    const handleFinish = async (e : any) => {
        let data: any = {
            name: name,
            description: description,
            members: members,
            dueDate: deadline,
            status: status,
        };
        const taskData = new FormData();
        for (const item in data) {
            if (['file', 'members'].includes(item)) {
                data[item]?.forEach((x: any) => {
                    if (item === 'members') taskData.append(item, x);
                    else taskData.append(item, x.originFileObj);
                });
            } else {
                taskData.append(item, data[item]);
            }
        }
        if(title === "Tạo nhiệm vụ"){
            try {
                await ApiService.createGeneralTask(taskData);
                Utils.showNotification('success', 'Tạo task mới thành công');
                router.push('/tasks');
            }
            catch (err) {
                Utils.showNotification('error', 'Tạo task mới thất bại , Vui lòng tạo lại')
            }
        }
        else{
            try{                
                await ApiService.updateGeneralTask(id,taskData);
                Utils.showNotification("success" , "Cập nhật task thành công");
                dispatch(setIsModalOpen(false))
            }
            catch(err){
                Utils.showNotification('error', 'Cập nhật dự án thất bại , vui lòng thử lại')
            }
        }
    }
    return (
        <TaskForm>
            <h1 className='heading'>{title}</h1>
            <div className="form_wrap">
                <Row>
                    <Col xs={24} sm={24} md={24} xl={16}>
                        <Form
                            layout="vertical"
                            initialValues={{
                                description: '',
                            }}
                            onFinish={handleFinish}
                        >
                            <Form.Item
                                name="title"
                                label="Tiêu đề nhiệm vụ"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập tiêu đề nhiệm vụ!',
                                    }
                                ]}
                            >
                                <Input value={name} onChange={e => setName(e.target.value)} placeholder={'Nhiệm vụ mới'} />
                            </Form.Item>
                            <Form.Item
                                name="description"
                                label="Mô tả nhiệm vụ"
                                rules={[{ required: true, message: 'Vui lòng nhập mô tả nhiệm vụ!' }]}
                            >
                                <ReactQuill
                                    modules={modules}
                                    theme="snow"
                                    value={description}
                                    placeholder="Mô tả"
                                // onChange={e => setDescription(e.target.value)}
                                />
                            </Form.Item>
                            <Form.Item
                                name="project"
                                label="Tên dự án"
                            >
                                <Select
                                    placeholder="Chọn dự án"
                                >
                                    {
                                        projectList &&
                                        projectList.map(project => (
                                            <Option>
                                                {project}
                                            </Option>
                                        ))
                                    }
                                </Select>
                            </Form.Item>
                            <Form.Item
                                name="members"
                                label="Người thực hiện"
                                placeholder="Thêm người nhận"
                                rules={[{ required: true, message: 'Vui lòng chọn người nhận!' }]}
                            >
                                <Select
                                    mode="multiple"
                                    style={{ width: '100%' }}
                                    placeholder="Chọn người tham gia"
                                    optionLabelProp="label"
                                    onChange={value => setMembers(value)}
                                >
                                    {member && member.map(person => (
                                        <Option value={person.fullName} label={person.fullName}>
                                            {person.fullName}
                                        </Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            <Form.Item>
                                <div className="switch-date__btn">
                                    <span className="mr-2">Chuyển chế độ thiết lập thời gian</span>
                                    <Switch defaultChecked />
                                </div>
                            </Form.Item>
                            <Form.Item
                                label="Range Date"
                                rules={[{ required: true, message: 'Vui lòng set up thời gian công việc!' }]}
                            >
                                <RangePicker
                                    showTime
                                    onChange={e => setDeadLine(e[1].format())}
                                />
                            </Form.Item>
                            <Form.Item
                                label="File đính kèm"
                                name="file"
                            >
                                <Upload.Dragger
                                    beforeUpload={file => {

                                    }}
                                    multiple
                                    style={{
                                        minHeight: 0,
                                        height: 32,
                                    }}
                                >
                                    <div className={'d-flex align-items-center justify-content-center'}>
                                        <InboxOutlined style={{ fontSize: 20, marginRight: 4 }} /> Kéo file vào đây...
                                    </div>
                                </Upload.Dragger>
                            </Form.Item>
                        </Form>
                    </Col>
                    <Col xs={24} sm={24} md={24} xl={8}>
                        <div className="mb-4">
                            <div className="mb-2">Thông tin</div>
                            <Card
                                className="card_container"
                                title="Chi tiết"
                                extra={
                                    <Select
                                        defaultValue="Đang chờ"
                                        style={{ width: 120 }}
                                        onChange={value => setStatus(value)}
                                        options={[
                                            { value: 'Đang chờ', label: 'Đang chờ' },
                                            { value: 'Đang triển khai', label: 'Đang triển khai' },
                                            { value: 'Hoàn thành', label: 'Hoàn thành' },
                                            { value: 'Luu trữ', label: 'Luu trữ' },
                                        ]}
                                    />
                                }
                            >
                                <div className="label">Tóm tắt</div>
                                <div className="label">Members</div>
                                <div className="label">Thời hạn</div>
                                <Collapse
                                    items={[{ key: '1', label: 'Mô tả' }]}
                                />
                            </Card>
                            {
                                title === "Tạo nhiệm vụ" ? 
                                (<Button type="primary" onClick={handleFinish} className="btn_Submit">Hoàn tất</Button>) 
                                : 
                                (<Button type="primary" onClick={handleFinish} className="btn_Submit">Lưu</Button>)
                            }
                        </div>
                    </Col>
                </Row>
            </div>
        </TaskForm>
    )
}


export default FormWrap;