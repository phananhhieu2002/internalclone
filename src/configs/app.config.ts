import { DIR_LTR, NAV_TYPE_SIDE, SIDE_NAV_LIGHT } from '../constants/ThemeConstants';
import { env } from './environment.config';

export const APP_NAME = 'NIS';
export const API_BASE_URL = env?.API_ENDPOINT_URL;
export const AUTH_PREFIX_PATH = '/auth';
export const APP_TOKEN_KEY = 'auth_token';
export const WAKA_APP_ID = 'VGVnrxIdoc4Kidd5kcwddnuW';

export const THEME_CONFIG = {
    navCollapsed: false,
    sideNavTheme: SIDE_NAV_LIGHT,
    locale: 'en',
    navType: NAV_TYPE_SIDE,
    topNavColor: '#3e82f7',
    headerNavColor: '',
    mobileNav: false,
    currentTheme: 'light',
    direction: DIR_LTR,
    isCordova: false,
};
